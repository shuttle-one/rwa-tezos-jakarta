<? include '../include/authen.php';?>
<? include '../api/function.php';?>
<?
$menuid = 4;
$sectionid = $_SESSION['sectionid'];

$wallet = getWallet($sectionid);

if ($wallet['code']=='0') { //---- SUCCESS
  $summary = $wallet['summary'];
  $currency = $wallet['currency'];
  $szoAmount = $wallet['xse'];
}

$arr = getTransactions($sectionid);
if ($arr['code']=='0') {
  $trans = $arr['data']['transection'];
}

?>
<!DOCTYPE html>
<html lang="en">
  <? include '../include/head.php'; ?>

  <body>
    <!-- Loader starts-->
    <? include '../include/loader.php'; ?>
    <!-- Loader ends-->
    <!-- page-wrapper Start-->
    <div class="page-wrapper">
      <!-- Page Header Start-->
      <? include '../include/top_bar.php'; ?>
      <!-- Page Header Ends -->
      <!-- Page Body Start-->
      <div class="page-body-wrapper">
        <!-- Page Sidebar Start-->
        <? include '../include/left_bar.php'; ?>
        <!-- Page Sidebar Ends-->
        <!-- Right sidebar Start-->
        <? include '../include/right_bar.php'; ?>
        <!-- Right sidebar Ends-->
        <div class="page-body">
        <br>
          <!-- <div class="page-header"> -->
          
            <div class="container-fluid">
              <div class="edit-profile">
                <div class="row">

                  <div class="col-sm-6">
                    <div class="card">
                      <div class="card-header">
                        <h5>Balance</h5>

                      </div>

                      <div class="card-body">
                        <div class="row">
                          <div class="col-md-8">
                            <h5><?=$summary//substr($summary, 0, -2) ?> <?=$currency?></h5>
                            
                          </div>
                        </div>
                      </div>
                    </div>
                  </div>

                  <div class="col-sm-6">
                    <div class="card">
                      <div class="card-header">
                        <h5>Maintain </h5>
                      </div>

                      <div class="card-body">
                        <div class="row">
                          <div class="col-md-8">
                            <h5><?=$szoAmount?></h5>
                          </div>
                        </div>
                      </div>
                    </div>
                  </div>

                  <!-- <div class="col-sm-3">
                    <div class="card">
                      <div class="card-header">
                        <a href="wallet_topup.php" class="btn btn-success">
                          TOPUP                        
                        </a>

                        <img src='../assets/imgs/visa.jpg' class='col-sm-12'>

                      </div>
                    </div>
                  </div>

                  <div class="col-sm-3">
                    <div class="card">
                      <div class="card-header">
                        <a href="wallet_withdraw.php" class="btn btn-primary">
                          WITHDRAW                       
                        </a>
                        <img src='../assets/imgs/withdraw.png' class='col-sm-12'>
                      </div>

                    </div>
                  </div> -->

                  <div class="col-lg-12">
                    <div class="card">
                      <div class="card-header">
                        <h5>Last Transactions</h5>
                      </div>
                      <div class="card-body">
                        <div class="table-responsive">
                          <table class="display" id="basic-2">
                            <thead>
                              <tr>
                                <th scope="col">#</th>
                                <th scope="col">Status</th>
                                <th scope="col">Amount</th>
                                <th scope="col">Date</th>
                                <th scope="col"></th>
                              </tr>
                            </thead>
                            <tbody>
                              <? foreach ($trans as $t) { 
                                $badge_color = "success";
                                $inout_icon = "<i data-feather=\"arrow-down-left\"></i>";

                                if (strtolower($t['detail'])=='fail') {
                                  $badge_color = "danger";
                                }else if (strtolower($t['detail'])=='success') {
                                  $badge_color = "success";
                                }else if (strtolower($t['detail'])=='cancel') {
                                  $badge_color = "warning";
                                }

                                if (strtolower($t['type'])=='in') {
                                  $inout_icon = "<i data-feather=\"arrow-down-left\"></i>";
                                } else if (strtolower($t['type'])=='out') {
                                  $inout_icon = "<i data-feather=\"arrow-up-right\"></i>";
                                }
                              ?>
                              <tr>
                                <th scope="row"><?=$inout_icon?></th>
                                <td>

                                  <span class="badge badge-pill badge-<?=$badge_color?>"> <?=$t['detail']?></span> 

                                </td>
                                <td><?=$t['amount']?></td>
                                <td><?=$t['time']?></td>
                                <td><a href="https://etherscan.io/tx/<?=$t['txHash']?>" target="_blank"><?=substr($t['txHash'],0,10)?> .. </a></td>
                              </tr>
                            <? } ?>


                            </tbody>
                          </table>
                        </div>
                      </div>
                    </div>
                  </div>
                </div>
              </div>
            </div>
          <!-- </div> -->

          <!-- Container-fluid Ends-->
        </div>
        <!-- footer start-->
        <? include '../include/footer.php'; ?>
      </div>
    </div>
    <!-- latest jquery-->
    <? include '../include/bottom_script.php'; ?>
    <!-- Plugin used-->
    <script>
      <? if ($arr['code']==2) { ?>
        alert("<?=$arr['data']?>");
        window.location.href="../login/logout.php";
      <?}?>
    </script>
  </body>
</html>