<footer class="footer">
          <div class="container-fluid">
            <div class="row">
              <div class="col-md-6 footer-copyright">
                <p class="mb-0">© Calista Finance</p>
              </div>
              <div class="col-md-6">
                <p class="pull-right mb-0">Powered by ShuttleOne</p>
              </div>
            </div>
          </div>
        </footer>