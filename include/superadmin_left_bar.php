<? if ($_SESSION['usertype']==5) { ?>

<div class="page-sidebar">
  <div class="main-header-left d-none d-lg-block">
    <div class="logo-wrapper"><a href="index.html"><img src="../assets/images/endless-logo.png" alt=""></a></div>
  </div>
  <div class="sidebar custom-scrollbar">

    <? include '../include/profile.php'; ?>

    <ul class="sidebar-menu">

      <li><a class="sidebar-header" href="../_superadmin/loan_waitrepay.php"><i data-feather="home"></i><span> Loan Wait Repay</span></a></li>

      <li><a class="sidebar-header" href="../login/logout.php"><i data-feather="power"></i><span> Sign Out</span></a></li>

        <hr>
        <span style="color: white">Powered by ShuttleOne</span>
        
    </ul>
  </div>
</div>

<? } else { ?>
<div class="page-sidebar">
    <div class="main-header-left d-none d-lg-block">
      <div class="logo-wrapper"><a href="index.html"><img src="../assets/images/endless-logo.png" alt=""></a></div>
    </div>
    <div class="sidebar custom-scrollbar">

      <? include '../include/profile.php'; ?>

      <ul class="sidebar-menu">

        <li><a class="sidebar-header" href="../_superadmin/scoring_list.php"><i data-feather="home"></i><span> Scoring List</span></a></li>

        <li><a class="sidebar-header" href="../_superadmin/loan_list.php"><i data-feather="home"></i><span> Loan Application</span></a></li>

        <li><a class="sidebar-header" href="../_superadmin/loan_approved.php"><i data-feather="home"></i><span> Loan Approved</span></a></li>

        <!-- <li><a class="sidebar-header" href="../_superadmin/kyc_list.php"><i data-feather="home"></i><span> Public KYC</span></a></li> -->

        <li><a class="sidebar-header" href="../_superadmin/loan_widget.php"><i data-feather="home"></i><span> Application from widget</span></a></li>

        <hr>
        <li><a class="sidebar-header" href="../_superadmin/loan_error.php"><i data-feather="home"></i><span> Loan Error (Beta)</span></a></li>
        <li><a class="sidebar-header" href="../_superadmin/loan_expired.php"><i data-feather="home"></i><span> Loan Expired (Beta)</span></a></li>
        <hr>

        <li><a class="sidebar-header" href="../login/logout.php"><i data-feather="power"></i><span> Sign Out</span></a></li>

        <hr>
        <span style="color: white">Powered by ShuttleOne</span>


      </ul>
    </div>
  </div>
<? } ?>