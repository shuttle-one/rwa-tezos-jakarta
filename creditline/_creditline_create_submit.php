<? include '../include/authen.php'; ?>
<? require_once ('../api/function.php'); ?>
<?
include '../include/database.php';
$db = new Database();  
$db->connect();


$docid = $_REQUEST['docid'];
$loan_amount = $_REQUEST['amount'];
$currency = $_REQUEST['currency'];
$wallet_addr = $_SESSION['useraddress'];
$sectionid = $_SESSION['sectionid'];
$contractid = '';

$err = '';

//----- CREATE CONTRACT
$param = array(
	"documentid" => $docid,
	"amount" => $loan_amount,
	"currency" => $currency,
	"userid" => $_SESSION['userid'],
	"apr" => 0,
	);

if ($db->insert("loan_contract_v2", $param))
{
	$res = $db->getResult();
	$contractid = $res[0];
}


if ($contractid == ''){
	$err = "Create Contract fail ";
	echo $err;
	return;
}

//----- CREATE TOKEN
$param = array(
	"docid" => $docid,
	"doctype" => 0,
	"loanid" => $contractid,
	"userid" => $_SESSION['userid'],
	);

if (!$db->insert("loan_token", $param))
{
	$err = "Fail Create Token";
	echo $err;
	return;
}

$tokens = $db->getResult();
$tokenid = $tokens[0];

if ($tokenid == ''){
	$err = "Fail Create Token";
	echo $err;
	return;
}

// $r = createLoan($sectionid, $tokenid, $docid, $contractid, $loan_amount, "test");

if ($r['code']==0){
	$txhash = $r['data']['txHash'];

	$sql = "update loan_contract_v2 set txhash='$txhash',status=1 where id=$contractid";
	$db->sql($sql);

}else {
	$err = $r['data'];
	echo "Error from server : " . $err;
	return;
}

if ($err != '') {
	echo " Fail " . $err;
?>
<script>
	window.location.href = "../error/error.php?e=$err&bp=../creditline/creditline_list.php";
</script>
<?
	
} else {
	?>
<script>
	alert('Success');
	window.location.href = "creditline_list.php";
</script>
	<?
}


?>