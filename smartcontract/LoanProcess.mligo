// This is an implementation of the FA1.2 specification in PascaLIGO
// (Pair (Pair {} "tz1djN1zPWUYpanMS1YhKJ2EmFSYs6qjf4bW") 0)
// https://better-call.dev/jakartanet/KT1UCij84mJXBP47rEpgcZLZCUFXnykqZ96h/operations

type amountNat is nat;
type id is int;


// type approveAuditParams is (address, "spender", int, "tokenid", int, "credit", int, "score");
type approveLegalParams is michelson_pair(address, "spender", int, "tokenid");
type auditParamData1 is michelson_pair(address, "spender", int , "tokenid");
type auditParamData2 is michelson_pair(int, "credit", int, "score");

type approve_data is record
    tokenid : int;
    legalAddress : address;
    auditAddress : address;
    credit: int;
    score: int;
end

type action is
| ApproveLegal of approveLegalParams
| ApproveAudit of (auditParamData1 * auditParamData2)
// | TestParam of (testParamData1 * testParamData2)
// (address * int * int * int)

type contract_storage is record
  owner: address;
  ledger: approve_data;
end

// function testParam (const spender : address ; const tokenid: int; const credit: int; const score: int; var s : contract_storage) : contract_storage is
//  begin
//   // If sender is the spender approving is not necessary
//   if sender =/= s.owner then failwith("You must be the owner of the contract to mint tokens");
//   else block {
//     const aa = 5;
//   }
// end with s

function approveLegal (const spender : address ; const tokenid : int ; var s : contract_storage) : contract_storage is
 begin
  // If sender is the spender approving is not necessary
  if sender =/= s.owner then failwith("You must be the owner of the contract to mint tokens");
  else block {

      var dst: approve_data := record 
        tokenid = 0;
        legalAddress = spender;
        auditAddress = spender;
        credit = 0;
        score = 0;
      end;
    
      dst.legalAddress := spender;
      dst.tokenid := tokenid;

      s.ledger := dst;

    // const src: account = get_force(sender, s.ledger);
    // src.allowances[spender] := value;
    // s.ledger[sender] := src; // Not sure if this last step is necessary
  }
end with s

function approveAudit (const spender : address ; const tokenid : int ; const credit: int; const score: int; var s : contract_storage) : contract_storage is
 begin
  // If sender is the spender approving is not necessary
  if sender =/= s.owner then failwith("You must be the owner of the contract to mint tokens");
  else block {

      var dst: approve_data := record 
        tokenid = 0;
        legalAddress = spender;
        auditAddress = spender;
        credit = 0;
        score = 0;
      end;
    
      dst.auditAddress := spender;
      dst.credit := credit;
      dst.score := score;
      dst.tokenid := tokenid;

      s.ledger := dst;

    // const src: account = get_force(sender, s.ledger);
    // src.allowances[spender] := value;
    // s.ledger[sender] := src; // Not sure if this last step is necessary
  }
 end with s




function main (const p : action ; const s : contract_storage) :
  (list(operation) * contract_storage) is
 block { 
   // Reject any transaction that try to transfer token to this contract
   if amount =/= 0tz then failwith ("This contract do not accept token");
   else skip;
  } with case p of
  | ApproveLegal(n) -> ((nil : list(operation)), approveLegal(n.0, n.1, s))
  | ApproveAudit(n) -> ((nil : list(operation)), approveAudit(n.0.0, n.0.1, n.1.0, n.1.1, s))
//   | TestParam(n) -> ((nil : list(operation)), testParam(n.0.0, n.0.1, n.1.0, n.1.1, s))
 end


 record [
    owner = ("tz1atXv46HHqjJfQzoios6TVpaDiep6JRr1p" : address);
    ledger = record [ 
       tokenid= 0;
       legalAddress = ("tz1atXv46HHqjJfQzoios6TVpaDiep6JRr1p" : address);
       auditAddress = ("tz1atXv46HHqjJfQzoios6TVpaDiep6JRr1p" : address);
       credit= 0;
       score= 0;
     ]
 ]