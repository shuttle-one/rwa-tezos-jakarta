(* https://better-call.dev/jakartanet/KT1N8iC991ks7bFEQ3CRSmFrzcJs3TSVZZsb/operations *)

type trusted is address;
type amt is nat;

type account is
  record [
    balance         : amt;
    allowances      : map (trusted, amt);
  ]

type token_metadata is
  record [
      token_id : nat;
      token_info : map (string, bytes);
  ]

(* contract storage *)
type storage is
  record [
    owner: address;
    minter: address;
    totalSupply     : amt;
    ledger          : big_map (address, account);
    metadata  : big_map (string, bytes);
    token_metadata : big_map(nat,token_metadata);
  ]

(* define return for readability *)
type return is list (operation) * storage

(* define noop for readability *)
const noOperations : list (operation) = nil;

(* Inputs *)
type transferParams is michelson_pair(address, "from", michelson_pair(address, "to", amt, "value"), "")
type approveParams is michelson_pair(trusted, "spender", amt, "value")
type balanceParams is michelson_pair(address, "owner", contract(amt), "")
type allowanceParams is michelson_pair(michelson_pair(address, "owner", trusted, "spender"), "", contract(amt), "")
type totalSupplyParams is (unit * contract(amt))

(* Valid entry points *)
type entryAction is
  | Transfer of transferParams
  | Approve of approveParams
  | GetBalance of balanceParams
  | GetAllowance of allowanceParams
  | GetTotalSupply of totalSupplyParams
  | Mint of (address * amt)
  | Burn of (address * amt)
   |AddMinter of (address)


function addMinter(const newMinter:address; var s : storage) : storage is
 begin
    if Tezos.get_sender() =/= s.owner 
    then failwith("You must be the owner of the contract to add minter")
    else  s.minter := newMinter;
    
 end with s 

// Mint tokens into the owner balance
// Preconditions:
//  The sender is the owner of the contract
// Postconditions:
//  The minted tokens are added in the balance of the owner
//  The totalSupply is increased by the amountNat of minted token
function mint (const mintTo: address; const value : amt ; var s : storage) : storage is
 begin
  // If the sender is not the owner fail
//  if sender =/= s.owner then failwith("You must be the owner of the contract to mint tokens");

  if s.minter =/= Tezos.get_sender() then failwith("You must be the minter of the contract to mint tokens");
  // else
  
  if s.minter =/= Tezos.get_sender() then  
    block {
    var mintAccount: account := record[
        balance = 0n;
        allowances = (map [] : map(address, amt));
    ];
    case s.ledger[mintTo] of [
    | None -> skip
    | Some(n) -> mintAccount := n
    ];

    // Update the owner balance
    mintAccount.balance := mintAccount.balance + value;
    s.ledger[mintTo] := mintAccount;
    s.totalSupply := s.totalSupply + value;
  }
 end with s

// Burn tokens from the owner balance
// Preconditions:
//  The owner have the required balance to burn
// Postconditions:
//  The burned tokens are subtracted from the balance of the owner
//  The totalSupply is decreased by the amountNat of burned token 
function burn (const burnTo:address; const value : amt ; var s : storage) : storage is
 begin
  // If the sender is not the owner fail
//  if sender =/= s.owner then failwith("You must be the owner of the contract to burn tokens");
  if s.minter =/= Tezos.get_sender() then failwith("You must be the owner of the contract to burn tokens"); 
  
  if s.minter = Tezos.get_sender() then
    block {
    var burnAccount: account := record [
            balance = 0n;
            allowances = (map [] : map(address, amt));
        ];
        
    case s.ledger[burnTo] of [
    | None -> skip
    | Some(n) -> burnAccount := n
    ];

    // Check that the owner can spend that much
    if value > burnAccount.balance 
    then failwith ("Owner balance is too low")
    else skip;

    // Update the owner balance
    // Using the abs function to convert int to nat
    burnAccount.balance := abs(burnAccount.balance - value);
    s.ledger[burnTo] := burnAccount;
    s.totalSupply := abs(s.totalSupply - value);
  }
 end with s

(* Helper function to get account *)
function getAccount (const addr : address; const s : storage) : account is
  block {
    var acct : account :=
      record [
        balance    = 0n;
        allowances = (map [] : map (address, amt));
      ];
    case s.ledger[addr] of [
      None -> skip
    | Some(instance) -> acct := instance
    ];
  } with acct

(* Helper function to get allowance for an account *)
function getAllowance (const ownerAccount : account; const spender : address; const s : storage) : amt is
  case ownerAccount.allowances[spender] of [
    Some (amt) -> amt
  | None -> 0n
  ];

(* Transfer token to another account *)
function transfer (const from_ : address; const to_ : address; const value : amt; var s : storage) : return is
  block {

    (* Retrieve sender account from storage *)
    var senderAccount : account := getAccount(from_, s);

    (* Balance check *)
    if senderAccount.balance < value then
      failwith("NotEnoughBalance")
    else skip;

    (* Check this address can spend the tokens *)
    if from_ =/= Tezos.get_sender() then block {
      const spenderAllowance : amt = getAllowance(senderAccount, Tezos.get_sender(), s);

      if spenderAllowance < value then
        failwith("NotEnoughAllowance")
      else skip;

      (* Decrease any allowances *)
      senderAccount.allowances[Tezos.get_sender()] := abs(spenderAllowance - value);
    } else skip;

    (* Update sender balance *)
    senderAccount.balance := abs(senderAccount.balance - value);

    (* Update storage *)
    s.ledger[from_] := senderAccount;

    (* Create or get destination account *)
    var destAccount : account := getAccount(to_, s);

    (* Update destination balance *)
    destAccount.balance := destAccount.balance + value;

    (* Update storage *)
    s.ledger[to_] := destAccount;

  } with (noOperations, s)

(* Approve an amt to be spent by another address in the name of the sender *)
function approve (const spender : address; const value : amt; var s : storage) : return is
  block {

    (* Create or get sender account *)
    var senderAccount : account := getAccount(Tezos.get_sender(), s);

    (* Get current spender allowance *)
    const spenderAllowance : amt = getAllowance(senderAccount, spender, s);

    (* Prevent a corresponding attack vector *)
    if spenderAllowance > 0n and value > 0n then
      failwith("UnsafeAllowanceChange")
    else skip;

    (* Set spender allowance *)
    senderAccount.allowances[spender] := value;

    (* Update storage *)
    s.ledger[Tezos.get_sender()] := senderAccount;

  } with (noOperations, s)

(* View function that forwards the balance of source to a contract *)
function getBalance (const owner : address; const contr : contract(amt); var s : storage) : return is
  block {
    const ownerAccount : account = getAccount(owner, s);
  } with (list [Tezos.transaction(ownerAccount.balance, 0tz, contr)], s)

(* View function that forwards the allowance amt of spender in the name of tokenOwner to a contract *)
function getAllowance (const owner : address; const spender : address; const contr : contract(amt); var s : storage) : return is
  block {
    const ownerAccount : account = getAccount(owner, s);
    const spenderAllowance : amt = getAllowance(ownerAccount, spender, s);
  } with (list [Tezos.transaction(spenderAllowance, 0tz, contr)], s)

(* View function that forwards the totalSupply to a contract *)
function getTotalSupply (const contr : contract(amt); var s : storage) : return is
  block {
    skip
  } with (list [Tezos.transaction(s.totalSupply, 0tz, contr)], s)

(* Main entrypoint *)
function main (const action : entryAction; var s : storage) : return is
  case action of [
    | Transfer(params) -> transfer(params.0, params.1.0, params.1.1, s)
    | Approve(params) -> approve(params.0, params.1, s)
    | GetBalance(params) -> getBalance(params.0, params.1, s)
    | GetAllowance(params) -> getAllowance(params.0.0, params.0.1, params.1, s)
    | GetTotalSupply(params) -> getTotalSupply(params.1, s)
    | Mint(n) -> ((nil : list(operation)), mint(n.0,n.1, s))
    | Burn(n) -> ((nil : list(operation)), burn(n.0,n.1, s))
    | AddMinter(n) -> ((nil : list(operation)),addMinter(n,s))
  ];


// ================= STORAGE ================
 record [
     owner = ("tz1atXv46HHqjJfQzoios6TVpaDiep6JRr1p" : address);
     minter = ("tz1atXv46HHqjJfQzoios6TVpaDiep6JRr1p" : address);
     totalSupply = 1n;
ledger = big_map [
("tz1atXv46HHqjJfQzoios6TVpaDiep6JRr1p" : address) -> record [
balance = 1n;
allowances = map [
("tz1atXv46HHqjJfQzoios6TVpaDiep6JRr1p" : address) -> 1n;]]];
metadata = big_map[
   ("data")-> 0x7b20226e616d65223a202253687574746c654f6e6520546f6b656e20436f6e7472616374222c20226465736372697074696f6e223a20224641312e3220496d706c656d656e746174696f6e206f6620535a4f222c2022617574686f7273223a205b225a6875616e67203c7a6875616e674073687574746c656f6e652e6e6574776f726b3e225d2c2022686f6d6570616765223a202268747470733a2f2f73687574746c656f6e652e6e6574776f726b222c2022696e7465726661636573223a205b2022545a49502d3030372d323032322d30382d3233225d207d;
  ("")->0x74657a6f732d73746f726167653a64617461;
];
token_metadata = big_map[
   0n->record[
   token_id = 0n;
   token_info = map[("name")-> 0x53687574746c654f6e6520546f6b656e;
   ("decimals")->0x36;
   ("symbol")-> 0x535a4f;
   ("icon")->0x68747470733a2f2f7261772e67697468756275736572636f6e74656e742e636f6d2f747275737477616c6c65742f6173736574732f6d61737465722f626c6f636b636861696e732f657468657265756d2f6173736574732f3078363038366235324361623435323262344230453861463943336232633562383939344333366261362f6c6f676f2e706e67;]]
];
 ]
 