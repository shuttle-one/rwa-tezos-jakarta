<? include '../include/superadmin_authen.php';?>
<?
$menuid = 1;
$userid = $_SESSION['userid'];
$config = include ('../api/config.php');

$tokenid = $_REQUEST['id'];

include '../include/database.php';
$db = new Database();  
$db->connect();

$sql = "SELECT t.tokenid, t.loanid, d.title, d.payperiod, DATE_ADD(c.active_date, INTERVAL d.payperiod DAY) as expiredate,c.* FROM `loan_contract_v2` c left join loan_documents_v2 d on c.documentid=d.id left join loan_token t on c.id=t.loanid where DATE_ADD(c.active_date, INTERVAL d.payperiod DAY) < date(now()) and t.tokenid=$tokenid and c.status=2";

$db->sql($sql);
$res = $db->getResult();

?>
<!DOCTYPE html>
<html lang="en">

<? include '../include/head.php'; ?>

<style>
.modal {
    display:    none;
    position:   fixed;
    z-index:    1000;
    top:        0;
    left:       0;
    height:     100%;
    width:      100%;
    background: rgba( 255, 255, 255, .8 ) 
                url('http://i.stack.imgur.com/FhHRx.gif') 
                50% 50% 
                no-repeat;
}

/* When the body has the loading class, we turn
   the scrollbar off with overflow:hidden */
body.loading .modal {
    overflow: hidden;   
}

/* Anytime the body has the loading class, our
   modal element will be visible */
body.loading .modal {
    display: block;
}
</style>


  <body>
    <!-- Loader starts-->
    <? include '../include/loader.php'; ?>
    <!-- Loader ends-->
    <!-- page-wrapper Start-->
    <div class="page-wrapper">
      <!-- Page Header Start-->
      <? include '../include/top_bar.php'; ?>
      <!-- Page Header Ends -->
      <!-- Page Body Start-->
      <div class="page-body-wrapper">
        <!-- Page Sidebar Start-->
        <? include '../include/superadmin_left_bar.php'; ?>
        <!-- Page Sidebar Ends-->
        <!-- Right sidebar Start-->
        <? include '../include/right_bar.php'; ?>
        <!-- Right sidebar Ends-->
        <div class="page-body">
          <div class="container-fluid">
            
            <? include '../include/header_space.php';?>
              <? if (count($res)!=1) { ?>
              <div class="row">
                <h5>Not found loan application</h5>
              </div>
              <? } else {?>
              <div class="row">
                <div class="col">
                  <div class="card">
                    <div class="card-header">
                      <div class="row">
                        <div class="col-md-6">
                          <h5 class="danger">Loan Applications Expired Detail</h5>
                        </div>
                        <div class="col-md-6 text-right">
                          <h6 id="ethaddr" class="danger" style="display: none;">0x00000000000</h6>
                          <button class="btn btn-sm btn-success" id="btn_connect">Connect wallet</button>
                        </div>

                      </div>

                    </div>
                    <div class="card-body">
                      <div class="table-responsive">
                        <table class="table table-border-horizontal">
                          <thead>
                            <tr class="text-center">
                              <!-- <th scope="col">#</th> -->
                              <th scope="col">Network</th>
                              <th scope="col">Summary Fund</th>
                              <th scope="col">szDAI</th>
                              <th scope="col">szUSDT</th>
                              <th scope="col">szUSDC</th>
                            </tr>
                          </thead>
                          <tbody>
                            <tr class="text-center">
                              <td><span id="ethNetwork"> Network .... </span></td>
                              <td><span id="sumSZ">0</span></td>
                              <td><span id="szdaiBalance">0</span></td>
                              <td><span id="szusdtBalance">0</span></td>
                              <td><span id="szusdcBalance">0</span></td>
                            </tr>
                          </tbody>
                        </table>
                      </div>

                      
                      <div class="row mt-4">
                        <div class="col-6">
                          <table class="table table-border-horizontal" style="background-color: #FAEBD7">
                            <tr>
                              <td colspan="2"><b><?=$res[0]['title']?></b></td> 
                            </tr>
                            <tr>
                              <td><b>Token ID </b></td>
                              <td><?=$tokenid?></td>
                            </tr>
                            <tr>
                              <td><b>Token Type </b></td>
                              <td><span id="_tokentype"></span></td>
                            </tr>
                            <tr>
                              <td><b>Principle </b></td>
                              <td><span id="_principle"></span></td>
                            </tr>
                            <tr>
                              <td><b>Interest</b></td>
                              <td><span id="_interest"></span></td>
                            </tr>
                            <tr>
                              <td><b>Token repay </b></td>
                              <td><span id="_szType"></span></td>
                            </tr>
                          </table>
                        </div>

                        
                          <div class="col-6 text-center">
                            <form class="form-horizontal" id="frm" method="post" action="loan_repay_submit.php">
                            <h5><b>Repay Amount</b></h5>
                            <input type="hidden" id="loanid" name="loanid" value="<?=$res[0]['loanid']?>">
                            <input type="hidden" id="amount" name="amount" value="0">
                            <input type="hidden" id="txhash" name="txhash" value="0">
                            <input type="hidden" id="userid" name="userid" value="0">

                            <input id="repayment" name="repayment" type="text" class="form-control form-control-lg text-right">
                            <button type="button" id="btn_submit" class="mt-2 btn btn-danger btn-block">
                              REPAY
                            </button>
                            </form>

                            <!-- <button id="btn_test" >test</button> -->
                          </div>
                       

                      </div>
                    </div>
                    

                  </div>
                </div>
                <h4 id="status" style="display: none">
                </h4>

                
              </div>
              <? } ?>
              
          </div>
          
        </div>
        <div class="modal"><!-- Place at bottom of page --></div>
        <!-- footer start-->
        <? include '../include/footer.php'; ?>
      </div>
    </div>
    <!-- latest jquery-->
    <? include '../include/bottom_script.php'; ?>

    <script src="https://cdn.jsdelivr.net/gh/ethereum/web3.js/dist/web3.min.js"></script>
  <script>

    var payAmount = 0;
    var tokenid = <?=$tokenid?>;


    var szToolsAddrMain = '0x9A58849765E186BDA183296fEdA6B3819f9C8fEa';
    var szToolsAddrKovan = '';
    var szToolsAddrGoerli = '0xBE459f2fdec8125ba17E6aC2d0498121eb9eceA5';

    // TODO: Replace your SimpleContract contract address here
    var daiPoolAddrKovan = '0x83B6A926B60DD0824a8dbd4330786121a30eFa64';
    var usdcPoolAddrKovan = '0xBcFD38565AbFA6E41e1BE2C3b692b5Bf96C321e3';
    var usdtPoolAddrKovan = '0x2DF6b6B9d13592635Dabcb5569d87326278bdfAa';

    var daiPoolAddrMain = '0xE29659A35260B87264eBf1155dD03B7DE17d9B26';
    var usdcPoolAddrMain = '0x93347FFA6020a3904790220E84f38594F35bac7D';
    var usdtPoolAddrMain = '0x1C69D1829A5970d85bCe8dD4A4f7f568DB492c81';

    var daiPoolAddrGoerli = '0xA42f921138f6d7EE5E6F3820f02454e15FEB1B0A';
    var usdcPoolAddrGoerli = '0x6e147535D957627099A0DA165a2aB917FD34d138';
    var usdtPoolAddrGoerli = '0x97f0394E5C544f73B69a3bbF1937f1B7739f8689';




    var DAIKovan = '0x194B3EcafCC274Faf324fA8e2dB9bcf320fc7d05';
    var USDTKovan = '0xfEE118A3D6107A3dCAC03F8CAa0D70d6D4fd1616';
    var USDCKovan = '0xD513fE16A68bD33fD5B61c9B8a920e49C29168dc';

    var DAIMain = '0x6B175474E89094C44Da98b954EedeAC495271d0F';
    var USDTMain = '0xdAC17F958D2ee523a2206206994597C13D831ec7';
    var USDCMain = '0xA0b86991c6218b36c1d19D4a2e9Eb0cE3606eB48';

    var DAIGoerli = '0x09bDbe368914AF6f76BC70d2Ce373E2c77AD94de';
    var USDTGoerli = '0x830b7E0fB577344d68563463C5b9aA6d2d2b090a';
    var USDCGoerli = '0x82EA2104fA9EdAc604a59CD93148452c2121dA23';

    var szoRewardAddrKovan = '0xCBd20759c12B77eF2f9963944dA87346038Af213';
    var szoRewardAddrGoerli = '0x758dFDB4478B4e35D6D38A8fcF693128b2b2b3C2';
    var szoRewardAddrMain = '0xD6f46bCA110bb74A4A121cd24DfD629145f2DbF8';

    var ratTokenAddrMain ='0x8bE308B0A4CB6753783E078cF12E4A236c11a85A';
    var ratTokenAddrKovan ='';
    var ratTokenAddrGoerli ='0xbce827633Bf633c2769c6D0bCd0B84123f2BC102';

    var repaymentAddrMain = '0x5A782703fC2090008c59a7734bE236506A40e45A';
    var repaymentAddrKovan = '';
    var repaymentAddrGoerli = '0x11Fff742399ffB641908AACF4FEA3baDaAb78280';//'0xbC623dee2d8d161f91849FE640e66F266D888968';

    var prefix;

    var abiReward =[
        {
          "constant": false,
          "inputs": [
            {
              "internalType": "address",
              "name": "_newOwner",
              "type": "address"
            },
            {
              "internalType": "string",
              "name": "newOwnerName",
              "type": "string"
            }
          ],
          "name": "addOwner",
          "outputs": [],
          "payable": false,
          "stateMutability": "nonpayable",
          "type": "function"
        },
        {
          "constant": false,
          "inputs": [
            {
              "internalType": "address",
              "name": "_contract",
              "type": "address"
            },
            {
              "internalType": "address",
              "name": "_wallet",
              "type": "address"
            }
          ],
          "name": "claimReward",
          "outputs": [
            {
              "internalType": "uint256",
              "name": "",
              "type": "uint256"
            }
          ],
          "payable": false,
          "stateMutability": "nonpayable",
          "type": "function"
        },
        {
          "inputs": [],
          "payable": false,
          "stateMutability": "nonpayable",
          "type": "constructor"
        },
        {
          "anonymous": false,
          "inputs": [
            {
              "indexed": false,
              "internalType": "address",
              "name": "newOwner",
              "type": "address"
            },
            {
              "indexed": false,
              "internalType": "string",
              "name": "name",
              "type": "string"
            }
          ],
          "name": "AddOwner",
          "type": "event"
        },
        {
          "anonymous": false,
          "inputs": [
            {
              "indexed": true,
              "internalType": "address",
              "name": "previousOwner",
              "type": "address"
            },
            {
              "indexed": true,
              "internalType": "address",
              "name": "newOwner",
              "type": "address"
            }
          ],
          "name": "OwnershipTransferred",
          "type": "event"
        },
        {
          "constant": false,
          "inputs": [
            {
              "internalType": "address",
              "name": "_owner",
              "type": "address"
            }
          ],
          "name": "removeOwner",
          "outputs": [],
          "payable": false,
          "stateMutability": "nonpayable",
          "type": "function"
        },
        {
          "anonymous": false,
          "inputs": [
            {
              "indexed": false,
              "internalType": "address",
              "name": "owner",
              "type": "address"
            }
          ],
          "name": "RemoveOwner",
          "type": "event"
        },
        {
          "constant": false,
          "inputs": [
            {
              "internalType": "address",
              "name": "_addr",
              "type": "address"
            },
            {
              "internalType": "bool",
              "name": "_act",
              "type": "bool"
            }
          ],
          "name": "setActivePools",
          "outputs": [],
          "payable": false,
          "stateMutability": "nonpayable",
          "type": "function"
        },
        {
          "constant": false,
          "inputs": [
            {
              "internalType": "address",
              "name": "_addr",
              "type": "address"
            }
          ],
          "name": "setPoolRewardAddr",
          "outputs": [],
          "payable": false,
          "stateMutability": "nonpayable",
          "type": "function"
        },
        {
          "constant": false,
          "inputs": [
            {
              "internalType": "uint256",
              "name": "_amount",
              "type": "uint256"
            }
          ],
          "name": "setRewardRatio",
          "outputs": [],
          "payable": false,
          "stateMutability": "nonpayable",
          "type": "function"
        },
        {
          "constant": false,
          "inputs": [
            {
              "internalType": "address",
              "name": "_addr",
              "type": "address"
            }
          ],
          "name": "setSZOAddr",
          "outputs": [],
          "payable": false,
          "stateMutability": "nonpayable",
          "type": "function"
        },
        {
          "constant": true,
          "inputs": [],
          "name": "amountPerToken",
          "outputs": [
            {
              "internalType": "uint256",
              "name": "",
              "type": "uint256"
            }
          ],
          "payable": false,
          "stateMutability": "view",
          "type": "function"
        },
        {
          "constant": true,
          "inputs": [],
          "name": "bonusPer30Day",
          "outputs": [
            {
              "internalType": "uint256",
              "name": "",
              "type": "uint256"
            }
          ],
          "payable": false,
          "stateMutability": "view",
          "type": "function"
        },
        {
          "constant": true,
          "inputs": [
            {
              "internalType": "address",
              "name": "_contract",
              "type": "address"
            },
            {
              "internalType": "address",
              "name": "_wallet",
              "type": "address"
            }
          ],
          "name": "getReward",
          "outputs": [
            {
              "internalType": "uint256",
              "name": "",
              "type": "uint256"
            }
          ],
          "payable": false,
          "stateMutability": "view",
          "type": "function"
        },
        {
          "constant": true,
          "inputs": [
            {
              "internalType": "address",
              "name": "_owner",
              "type": "address"
            }
          ],
          "name": "isOwner",
          "outputs": [
            {
              "internalType": "bool",
              "name": "",
              "type": "bool"
            }
          ],
          "payable": false,
          "stateMutability": "view",
          "type": "function"
        },
        {
          "constant": true,
          "inputs": [
            {
              "internalType": "address",
              "name": "",
              "type": "address"
            }
          ],
          "name": "lastTimeClaim",
          "outputs": [
            {
              "internalType": "uint256",
              "name": "",
              "type": "uint256"
            }
          ],
          "payable": false,
          "stateMutability": "view",
          "type": "function"
        },
        {
          "constant": true,
          "inputs": [],
          "name": "maxPerDay",
          "outputs": [
            {
              "internalType": "uint256",
              "name": "",
              "type": "uint256"
            }
          ],
          "payable": false,
          "stateMutability": "view",
          "type": "function"
        },
        {
          "constant": true,
          "inputs": [
            {
              "internalType": "uint256",
              "name": "",
              "type": "uint256"
            }
          ],
          "name": "pools",
          "outputs": [
            {
              "internalType": "address",
              "name": "",
              "type": "address"
            }
          ],
          "payable": false,
          "stateMutability": "view",
          "type": "function"
        },
        {
          "constant": true,
          "inputs": [
            {
              "internalType": "address",
              "name": "",
              "type": "address"
            }
          ],
          "name": "poolsRewardActive",
          "outputs": [
            {
              "internalType": "bool",
              "name": "",
              "type": "bool"
            }
          ],
          "payable": false,
          "stateMutability": "view",
          "type": "function"
        },
        {
          "constant": true,
          "inputs": [
            {
              "internalType": "address",
              "name": "",
              "type": "address"
            }
          ],
          "name": "poolsRewardIdx",
          "outputs": [
            {
              "internalType": "uint256",
              "name": "",
              "type": "uint256"
            }
          ],
          "payable": false,
          "stateMutability": "view",
          "type": "function"
        },
        {
          "constant": true,
          "inputs": [],
          "name": "rewardPerSec",
          "outputs": [
            {
              "internalType": "uint256",
              "name": "",
              "type": "uint256"
            }
          ],
          "payable": false,
          "stateMutability": "view",
          "type": "function"
        }
    ];

    var abiERC20 = [
      {
        "constant": true,
        "inputs": [
          {
            "internalType": "address",
            "name": "tokenOwner",
            "type": "address"
          },
          {
            "internalType": "address",
            "name": "spender",
            "type": "address"
          }
        ],
        "name": "allowance",
        "outputs": [
          {
            "internalType": "uint256",
            "name": "remaining",
            "type": "uint256"
          }
        ],
        "payable": false,
        "stateMutability": "view",
        "type": "function"
      },
      {
        "constant": false,
        "inputs": [
          {
            "internalType": "address",
            "name": "spender",
            "type": "address"
          },
          {
            "internalType": "uint256",
            "name": "tokens",
            "type": "uint256"
          }
        ],
        "name": "approve",
        "outputs": [
          {
            "internalType": "bool",
            "name": "success",
            "type": "bool"
          }
        ],
        "payable": false,
        "stateMutability": "nonpayable",
        "type": "function"
      },
      {
        "constant": true,
        "inputs": [
          {
            "internalType": "address",
            "name": "tokenOwner",
            "type": "address"
          }
        ],
        "name": "balanceOf",
        "outputs": [
          {
            "internalType": "uint256",
            "name": "balance",
            "type": "uint256"
          }
        ],
        "payable": false,
        "stateMutability": "view",
        "type": "function"
      },
      {
        "constant": true,
        "inputs": [],
        "name": "decimals",
        "outputs": [
          {
            "internalType": "uint256",
            "name": "",
            "type": "uint256"
          }
        ],
        "payable": false,
        "stateMutability": "view",
        "type": "function"
      },
      {
        "constant": false,
        "inputs": [
          {
            "internalType": "address",
            "name": "to",
            "type": "address"
          },
          {
            "internalType": "uint256",
            "name": "tokens",
            "type": "uint256"
          }
        ],
        "name": "transfer",
        "outputs": [
          {
            "internalType": "bool",
            "name": "success",
            "type": "bool"
          }
        ],
        "payable": false,
        "stateMutability": "nonpayable",
        "type": "function"
      },
      {
        "constant": false,
        "inputs": [
          {
            "internalType": "address",
            "name": "from",
            "type": "address"
          },
          {
            "internalType": "address",
            "name": "to",
            "type": "address"
          },
          {
            "internalType": "uint256",
            "name": "tokens",
            "type": "uint256"
          }
        ],
        "name": "transferFrom",
        "outputs": [
          {
            "internalType": "bool",
            "name": "success",
            "type": "bool"
          }
        ],
        "payable": false,
        "stateMutability": "nonpayable",
        "type": "function"
      }
        ];

    var abiPOOLS = [
        {
          "inputs": [],
          "payable": false,
          "stateMutability": "nonpayable",
          "type": "constructor"
        },
        {
          "anonymous": false,
          "inputs": [
            {
              "indexed": false,
              "internalType": "address",
              "name": "_addr",
              "type": "address"
            }
          ],
          "name": "AddPermit",
          "type": "event"
        },
        {
          "anonymous": false,
          "inputs": [
            {
              "indexed": true,
              "internalType": "address",
              "name": "_from",
              "type": "address"
            },
            {
              "indexed": false,
              "internalType": "uint256",
              "name": "amount",
              "type": "uint256"
            },
            {
              "indexed": false,
              "internalType": "uint256",
              "name": "interest",
              "type": "uint256"
            }
          ],
          "name": "Borrow",
          "type": "event"
        },
        {
          "anonymous": false,
          "inputs": [
            {
              "indexed": true,
              "internalType": "address",
              "name": "_from",
              "type": "address"
            },
            {
              "indexed": false,
              "internalType": "uint256",
              "name": "amount",
              "type": "uint256"
            }
          ],
          "name": "Deposit",
          "type": "event"
        },
        {
          "anonymous": false,
          "inputs": [
            {
              "indexed": false,
              "internalType": "address",
              "name": "_addr",
              "type": "address"
            }
          ],
          "name": "RemovePermit",
          "type": "event"
        },
        {
          "anonymous": false,
          "inputs": [
            {
              "indexed": true,
              "internalType": "address",
              "name": "",
              "type": "address"
            },
            {
              "indexed": true,
              "internalType": "uint256",
              "name": "contractID",
              "type": "uint256"
            },
            {
              "indexed": false,
              "internalType": "uint256",
              "name": "amount",
              "type": "uint256"
            }
          ],
          "name": "Repayment",
          "type": "event"
        },
        {
          "anonymous": false,
          "inputs": [
            {
              "indexed": false,
              "internalType": "uint256",
              "name": "_interest",
              "type": "uint256"
            }
          ],
          "name": "SetInterest",
          "type": "event"
        },
        {
          "anonymous": false,
          "inputs": [
            {
              "indexed": true,
              "internalType": "address",
              "name": "_from",
              "type": "address"
            },
            {
              "indexed": false,
              "internalType": "uint256",
              "name": "amount",
              "type": "uint256"
            }
          ],
          "name": "Withdraw",
          "type": "event"
        },
        {
          "anonymous": false,
          "inputs": [
            {
              "indexed": true,
              "internalType": "address",
              "name": "_from",
              "type": "address"
            },
            {
              "indexed": false,
              "internalType": "uint256",
              "name": "amount",
              "type": "uint256"
            }
          ],
          "name": "WithdrawInterest",
          "type": "event"
        },
        {
          "constant": true,
          "inputs": [],
          "name": "POOL_HARDCAP",
          "outputs": [
            {
              "internalType": "uint256",
              "name": "",
              "type": "uint256"
            }
          ],
          "payable": false,
          "stateMutability": "view",
          "type": "function"
        },
        {
          "constant": true,
          "inputs": [],
          "name": "SECPYEAR",
          "outputs": [
            {
              "internalType": "uint256",
              "name": "",
              "type": "uint256"
            }
          ],
          "payable": false,
          "stateMutability": "view",
          "type": "function"
        },
        {
          "constant": true,
          "inputs": [],
          "name": "TOKEN_POOLS",
          "outputs": [
            {
              "internalType": "string",
              "name": "",
              "type": "string"
            }
          ],
          "payable": false,
          "stateMutability": "view",
          "type": "function"
        },
        {
          "constant": false,
          "inputs": [
            {
              "internalType": "address",
              "name": "_addr",
              "type": "address"
            }
          ],
          "name": "addPermit",
          "outputs": [],
          "payable": false,
          "stateMutability": "nonpayable",
          "type": "function"
        },
        {
          "constant": true,
          "inputs": [
            {
              "internalType": "address",
              "name": "",
              "type": "address"
            }
          ],
          "name": "balance",
          "outputs": [
            {
              "internalType": "uint256",
              "name": "",
              "type": "uint256"
            }
          ],
          "payable": false,
          "stateMutability": "view",
          "type": "function"
        },
        {
          "constant": false,
          "inputs": [
            {
              "internalType": "uint256",
              "name": "amount",
              "type": "uint256"
            }
          ],
          "name": "borrow",
          "outputs": [
            {
              "internalType": "uint256",
              "name": "contractID",
              "type": "uint256"
            }
          ],
          "payable": false,
          "stateMutability": "nonpayable",
          "type": "function"
        },
        {
          "constant": true,
          "inputs": [],
          "name": "borrowInterest",
          "outputs": [
            {
              "internalType": "uint256",
              "name": "",
              "type": "uint256"
            }
          ],
          "payable": false,
          "stateMutability": "view",
          "type": "function"
        },
        {
          "constant": false,
          "inputs": [
            {
              "internalType": "uint256",
              "name": "amount",
              "type": "uint256"
            },
            {
              "internalType": "address",
              "name": "_addr",
              "type": "address"
            }
          ],
          "name": "borrowWithAddr",
          "outputs": [
            {
              "internalType": "uint256",
              "name": "contractID",
              "type": "uint256"
            }
          ],
          "payable": false,
          "stateMutability": "nonpayable",
          "type": "function"
        },
        {
          "constant": true,
          "inputs": [],
          "name": "catToken",
          "outputs": [
            {
              "internalType": "contract ERC20",
              "name": "",
              "type": "address"
            }
          ],
          "payable": false,
          "stateMutability": "view",
          "type": "function"
        },
        {
          "constant": true,
          "inputs": [],
          "name": "decimal",
          "outputs": [
            {
              "internalType": "uint256",
              "name": "",
              "type": "uint256"
            }
          ],
          "payable": false,
          "stateMutability": "view",
          "type": "function"
        },
        {
          "constant": false,
          "inputs": [
            {
              "internalType": "address",
              "name": "_from",
              "type": "address"
            },
            {
              "internalType": "uint256",
              "name": "amount",
              "type": "uint256"
            }
          ],
          "name": "depositSZToken",
          "outputs": [
            {
              "internalType": "bool",
              "name": "",
              "type": "bool"
            }
          ],
          "payable": false,
          "stateMutability": "nonpayable",
          "type": "function"
        },
        {
          "constant": false,
          "inputs": [
            {
              "internalType": "address",
              "name": "_from",
              "type": "address"
            },
            {
              "internalType": "uint256",
              "name": "amount",
              "type": "uint256"
            },
            {
              "internalType": "uint256",
              "name": "_term",
              "type": "uint256"
            }
          ],
          "name": "depositSZTokenTerm",
          "outputs": [
            {
              "internalType": "bool",
              "name": "",
              "type": "bool"
            }
          ],
          "payable": false,
          "stateMutability": "nonpayable",
          "type": "function"
        },
        {
          "constant": false,
          "inputs": [
            {
              "internalType": "address",
              "name": "_from",
              "type": "address"
            },
            {
              "internalType": "uint256",
              "name": "amount",
              "type": "uint256"
            }
          ],
          "name": "depositToken",
          "outputs": [
            {
              "internalType": "bool",
              "name": "",
              "type": "bool"
            }
          ],
          "payable": false,
          "stateMutability": "nonpayable",
          "type": "function"
        },
        {
          "constant": false,
          "inputs": [
            {
              "internalType": "address",
              "name": "_from",
              "type": "address"
            },
            {
              "internalType": "uint256",
              "name": "amount",
              "type": "uint256"
            },
            {
              "internalType": "uint256",
              "name": "_term",
              "type": "uint256"
            }
          ],
          "name": "depositTokenTerm",
          "outputs": [
            {
              "internalType": "bool",
              "name": "",
              "type": "bool"
            }
          ],
          "payable": false,
          "stateMutability": "nonpayable",
          "type": "function"
        },
        {
          "constant": true,
          "inputs": [
            {
              "internalType": "address",
              "name": "_addr",
              "type": "address"
            }
          ],
          "name": "getAllDepositIdx",
          "outputs": [
            {
              "internalType": "uint256[]",
              "name": "_idx",
              "type": "uint256[]"
            }
          ],
          "payable": false,
          "stateMutability": "view",
          "type": "function"
        },
        {
          "constant": true,
          "inputs": [
            {
              "internalType": "uint256",
              "name": "condIdx",
              "type": "uint256"
            }
          ],
          "name": "getBorrowData",
          "outputs": [
            {
              "internalType": "uint256[]",
              "name": "_data",
              "type": "uint256[]"
            }
          ],
          "payable": false,
          "stateMutability": "view",
          "type": "function"
        },
        {
          "constant": true,
          "inputs": [],
          "name": "getContractBorrow",
          "outputs": [
            {
              "internalType": "uint256[]",
              "name": "cons",
              "type": "uint256[]"
            }
          ],
          "payable": false,
          "stateMutability": "view",
          "type": "function"
        },
        {
          "constant": true,
          "inputs": [
            {
              "internalType": "uint256",
              "name": "idx",
              "type": "uint256"
            }
          ],
          "name": "getDepositDataIdx",
          "outputs": [
            {
              "internalType": "uint256[]",
              "name": "_data",
              "type": "uint256[]"
            }
          ],
          "payable": false,
          "stateMutability": "view",
          "type": "function"
        },
        {
          "constant": true,
          "inputs": [
            {
              "internalType": "address",
              "name": "_addr",
              "type": "address"
            }
          ],
          "name": "getInterestProfit",
          "outputs": [
            {
              "internalType": "uint256",
              "name": "",
              "type": "uint256"
            }
          ],
          "payable": false,
          "stateMutability": "view",
          "type": "function"
        },
        {
          "constant": true,
          "inputs": [],
          "name": "getMaxBorrowContract",
          "outputs": [
            {
              "internalType": "uint256",
              "name": "",
              "type": "uint256"
            }
          ],
          "payable": false,
          "stateMutability": "view",
          "type": "function"
        },
        {
          "constant": true,
          "inputs": [],
          "name": "getMaxDeposit",
          "outputs": [
            {
              "internalType": "uint256",
              "name": "",
              "type": "uint256"
            }
          ],
          "payable": false,
          "stateMutability": "view",
          "type": "function"
        },
        {
          "constant": true,
          "inputs": [
            {
              "internalType": "address",
              "name": "_addr",
              "type": "address"
            }
          ],
          "name": "getMaxDepositContract",
          "outputs": [
            {
              "internalType": "uint256",
              "name": "_max",
              "type": "uint256"
            }
          ],
          "payable": false,
          "stateMutability": "view",
          "type": "function"
        },
        {
          "constant": true,
          "inputs": [],
          "name": "getMaxInterestData",
          "outputs": [
            {
              "internalType": "uint256",
              "name": "",
              "type": "uint256"
            }
          ],
          "payable": false,
          "stateMutability": "view",
          "type": "function"
        },
        {
          "constant": true,
          "inputs": [
            {
              "internalType": "uint256",
              "name": "",
              "type": "uint256"
            }
          ],
          "name": "interests",
          "outputs": [
            {
              "internalType": "uint256",
              "name": "startTime",
              "type": "uint256"
            },
            {
              "internalType": "uint256",
              "name": "interest",
              "type": "uint256"
            }
          ],
          "payable": false,
          "stateMutability": "view",
          "type": "function"
        },
        {
          "constant": true,
          "inputs": [
            {
              "internalType": "address",
              "name": "_addr",
              "type": "address"
            }
          ],
          "name": "isPermit",
          "outputs": [
            {
              "internalType": "bool",
              "name": "",
              "type": "bool"
            }
          ],
          "payable": false,
          "stateMutability": "view",
          "type": "function"
        },
        {
          "constant": true,
          "inputs": [],
          "name": "loanBalance",
          "outputs": [
            {
              "internalType": "uint256",
              "name": "",
              "type": "uint256"
            }
          ],
          "payable": false,
          "stateMutability": "view",
          "type": "function"
        },
        {
          "constant": true,
          "inputs": [],
          "name": "maxContracBorrow",
          "outputs": [
            {
              "internalType": "uint256",
              "name": "",
              "type": "uint256"
            }
          ],
          "payable": false,
          "stateMutability": "view",
          "type": "function"
        },
        {
          "constant": true,
          "inputs": [],
          "name": "minimumTerm",
          "outputs": [
            {
              "internalType": "uint256",
              "name": "",
              "type": "uint256"
            }
          ],
          "payable": false,
          "stateMutability": "view",
          "type": "function"
        },
        {
          "constant": true,
          "inputs": [
            {
              "internalType": "address",
              "name": "",
              "type": "address"
            }
          ],
          "name": "permits",
          "outputs": [
            {
              "internalType": "bool",
              "name": "",
              "type": "bool"
            }
          ],
          "payable": false,
          "stateMutability": "view",
          "type": "function"
        },
        {
          "constant": true,
          "inputs": [
            {
              "internalType": "uint256",
              "name": "conIdx",
              "type": "uint256"
            }
          ],
          "name": "pricipleAndInterest",
          "outputs": [
            {
              "internalType": "uint256",
              "name": "principle",
              "type": "uint256"
            },
            {
              "internalType": "uint256",
              "name": "_int",
              "type": "uint256"
            }
          ],
          "payable": false,
          "stateMutability": "view",
          "type": "function"
        },
        {
          "constant": true,
          "inputs": [],
          "name": "profitCal",
          "outputs": [
            {
              "internalType": "contract ProfitCal",
              "name": "",
              "type": "address"
            }
          ],
          "payable": false,
          "stateMutability": "view",
          "type": "function"
        },
        {
          "constant": false,
          "inputs": [
            {
              "internalType": "uint256",
              "name": "amount",
              "type": "uint256"
            },
            {
              "internalType": "uint256",
              "name": "conIdx",
              "type": "uint256"
            }
          ],
          "name": "rePayment",
          "outputs": [
            {
              "internalType": "bool",
              "name": "",
              "type": "bool"
            }
          ],
          "payable": false,
          "stateMutability": "nonpayable",
          "type": "function"
        },
        {
          "constant": false,
          "inputs": [
            {
              "internalType": "uint256",
              "name": "amount",
              "type": "uint256"
            },
            {
              "internalType": "uint256",
              "name": "conIdx",
              "type": "uint256"
            },
            {
              "internalType": "address",
              "name": "_addr",
              "type": "address"
            }
          ],
          "name": "rePaymentWithWrap",
          "outputs": [
            {
              "internalType": "bool",
              "name": "",
              "type": "bool"
            }
          ],
          "payable": false,
          "stateMutability": "nonpayable",
          "type": "function"
        },
        {
          "constant": false,
          "inputs": [
            {
              "internalType": "address",
              "name": "_addr",
              "type": "address"
            }
          ],
          "name": "removePermit",
          "outputs": [],
          "payable": false,
          "stateMutability": "nonpayable",
          "type": "function"
        },
        {
          "constant": false,
          "inputs": [
            {
              "internalType": "bool",
              "name": "_set",
              "type": "bool"
            }
          ],
          "name": "setAdminControl",
          "outputs": [],
          "payable": false,
          "stateMutability": "nonpayable",
          "type": "function"
        },
        {
          "constant": false,
          "inputs": [
            {
              "internalType": "uint256",
              "name": "_newInterst",
              "type": "uint256"
            }
          ],
          "name": "setBorrowInterest",
          "outputs": [],
          "payable": false,
          "stateMutability": "nonpayable",
          "type": "function"
        },
        {
          "constant": false,
          "inputs": [
            {
              "internalType": "address",
              "name": "_addr",
              "type": "address"
            }
          ],
          "name": "setCatToken",
          "outputs": [],
          "payable": false,
          "stateMutability": "nonpayable",
          "type": "function"
        },
        {
          "constant": false,
          "inputs": [
            {
              "internalType": "uint256",
              "name": "_min",
              "type": "uint256"
            }
          ],
          "name": "setMinimumDeposit",
          "outputs": [],
          "payable": false,
          "stateMutability": "nonpayable",
          "type": "function"
        },
        {
          "constant": false,
          "inputs": [
            {
              "internalType": "uint256",
              "name": "amount",
              "type": "uint256"
            }
          ],
          "name": "setPoolHardcap",
          "outputs": [],
          "payable": false,
          "stateMutability": "nonpayable",
          "type": "function"
        },
        {
          "constant": false,
          "inputs": [
            {
              "internalType": "address",
              "name": "_addr",
              "type": "address"
            }
          ],
          "name": "setProfitCal",
          "outputs": [],
          "payable": false,
          "stateMutability": "nonpayable",
          "type": "function"
        },
        {
          "constant": true,
          "inputs": [],
          "name": "startPools",
          "outputs": [
            {
              "internalType": "uint256",
              "name": "",
              "type": "uint256"
            }
          ],
          "payable": false,
          "stateMutability": "view",
          "type": "function"
        },
        {
          "constant": true,
          "inputs": [],
          "name": "supplyInterest",
          "outputs": [
            {
              "internalType": "uint256",
              "name": "",
              "type": "uint256"
            }
          ],
          "payable": false,
          "stateMutability": "view",
          "type": "function"
        },
        {
          "constant": true,
          "inputs": [],
          "name": "token",
          "outputs": [
            {
              "internalType": "contract ERC20",
              "name": "",
              "type": "address"
            }
          ],
          "payable": false,
          "stateMutability": "view",
          "type": "function"
        },
        {
          "constant": true,
          "inputs": [],
          "name": "totalBorrow",
          "outputs": [
            {
              "internalType": "uint256",
              "name": "",
              "type": "uint256"
            }
          ],
          "payable": false,
          "stateMutability": "view",
          "type": "function"
        },
        {
          "constant": true,
          "inputs": [],
          "name": "totalClaimInterest",
          "outputs": [
            {
              "internalType": "uint256",
              "name": "",
              "type": "uint256"
            }
          ],
          "payable": false,
          "stateMutability": "view",
          "type": "function"
        },
        {
          "constant": true,
          "inputs": [],
          "name": "totalInterest",
          "outputs": [
            {
              "internalType": "uint256",
              "name": "",
              "type": "uint256"
            }
          ],
          "payable": false,
          "stateMutability": "view",
          "type": "function"
        },
        {
          "constant": true,
          "inputs": [],
          "name": "totalSupply",
          "outputs": [
            {
              "internalType": "uint256",
              "name": "",
              "type": "uint256"
            }
          ],
          "payable": false,
          "stateMutability": "view",
          "type": "function"
        },
        {
          "constant": true,
          "inputs": [],
          "name": "version",
          "outputs": [
            {
              "internalType": "uint256",
              "name": "",
              "type": "uint256"
            }
          ],
          "payable": false,
          "stateMutability": "view",
          "type": "function"
        },
        {
          "constant": false,
          "inputs": [
            {
              "internalType": "address",
              "name": "_to",
              "type": "address"
            },
            {
              "internalType": "uint256",
              "name": "amount",
              "type": "uint256"
            }
          ],
          "name": "withdraw",
          "outputs": [
            {
              "internalType": "uint256",
              "name": "",
              "type": "uint256"
            }
          ],
          "payable": false,
          "stateMutability": "nonpayable",
          "type": "function"
        },
        {
          "constant": false,
          "inputs": [
            {
              "internalType": "address",
              "name": "_to",
              "type": "address"
            }
          ],
          "name": "withdrawAll",
          "outputs": [
            {
              "internalType": "uint256",
              "name": "",
              "type": "uint256"
            }
          ],
          "payable": false,
          "stateMutability": "nonpayable",
          "type": "function"
        },
        {
          "constant": false,
          "inputs": [
            {
              "internalType": "address",
              "name": "_to",
              "type": "address"
            }
          ],
          "name": "withdrawAllStable",
          "outputs": [
            {
              "internalType": "bool",
              "name": "",
              "type": "bool"
            }
          ],
          "payable": false,
          "stateMutability": "nonpayable",
          "type": "function"
        },
        {
          "constant": false,
          "inputs": [
            {
              "internalType": "address",
              "name": "_to",
              "type": "address"
            }
          ],
          "name": "withdrawIntStable",
          "outputs": [
            {
              "internalType": "bool",
              "name": "",
              "type": "bool"
            }
          ],
          "payable": false,
          "stateMutability": "nonpayable",
          "type": "function"
        },
        {
          "constant": false,
          "inputs": [
            {
              "internalType": "address",
              "name": "_to",
              "type": "address"
            }
          ],
          "name": "withdrawInterest",
          "outputs": [
            {
              "internalType": "uint256",
              "name": "",
              "type": "uint256"
            }
          ],
          "payable": false,
          "stateMutability": "nonpayable",
          "type": "function"
        },
        {
          "constant": false,
          "inputs": [
            {
              "internalType": "address",
              "name": "_to",
              "type": "address"
            },
            {
              "internalType": "uint256",
              "name": "percent",
              "type": "uint256"
            }
          ],
          "name": "withdrawPercent",
          "outputs": [
            {
              "internalType": "uint256",
              "name": "",
              "type": "uint256"
            }
          ],
          "payable": false,
          "stateMutability": "nonpayable",
          "type": "function"
        },
        {
          "constant": false,
          "inputs": [
            {
              "internalType": "address",
              "name": "_to",
              "type": "address"
            },
            {
              "internalType": "uint256",
              "name": "percent",
              "type": "uint256"
            }
          ],
          "name": "withdrawPercentStable",
          "outputs": [
            {
              "internalType": "bool",
              "name": "",
              "type": "bool"
            }
          ],
          "payable": false,
          "stateMutability": "nonpayable",
          "type": "function"
        },
        {
          "constant": false,
          "inputs": [
            {
              "internalType": "address",
              "name": "_to",
              "type": "address"
            },
            {
              "internalType": "uint256",
              "name": "amount",
              "type": "uint256"
            }
          ],
          "name": "withdrawStable",
          "outputs": [
            {
              "internalType": "bool",
              "name": "",
              "type": "bool"
            }
          ],
          "payable": false,
          "stateMutability": "nonpayable",
          "type": "function"
        },
        {
          "constant": false,
          "inputs": [
            {
              "internalType": "uint256",
              "name": "amount",
              "type": "uint256"
            },
            {
              "internalType": "address",
              "name": "_contract",
              "type": "address"
            },
            {
              "internalType": "address",
              "name": "_to",
              "type": "address"
            }
          ],
          "name": "withdrawStupidUser",
          "outputs": [],
          "payable": false,
          "stateMutability": "nonpayable",
          "type": "function"
        }
      ]

    var abiSZToken = [
      {
        "inputs": [],
        "payable": false,
        "stateMutability": "nonpayable",
        "type": "constructor"
      },
      {
        "anonymous": false,
        "inputs": [
          {
            "indexed": false,
            "internalType": "address",
            "name": "newOwner",
            "type": "address"
          },
          {
            "indexed": false,
            "internalType": "string",
            "name": "name",
            "type": "string"
          }
        ],
        "name": "AddOwner",
        "type": "event"
      },
      {
        "anonymous": false,
        "inputs": [
          {
            "indexed": true,
            "internalType": "address",
            "name": "_tokenOwner",
            "type": "address"
          },
          {
            "indexed": true,
            "internalType": "address",
            "name": "_spender",
            "type": "address"
          },
          {
            "indexed": false,
            "internalType": "uint256",
            "name": "_amount",
            "type": "uint256"
          }
        ],
        "name": "Approval",
        "type": "event"
      },
      {
        "anonymous": false,
        "inputs": [
          {
            "indexed": true,
            "internalType": "address",
            "name": "_from",
            "type": "address"
          },
          {
            "indexed": false,
            "internalType": "uint256",
            "name": "_amount",
            "type": "uint256"
          }
        ],
        "name": "Deposit",
        "type": "event"
      },
      {
        "anonymous": false,
        "inputs": [
          {
            "indexed": true,
            "internalType": "address",
            "name": "previousOwner",
            "type": "address"
          },
          {
            "indexed": true,
            "internalType": "address",
            "name": "newOwner",
            "type": "address"
          }
        ],
        "name": "OwnershipTransferred",
        "type": "event"
      },
      {
        "anonymous": false,
        "inputs": [
          {
            "indexed": false,
            "internalType": "address",
            "name": "owner",
            "type": "address"
          }
        ],
        "name": "RemoveOwner",
        "type": "event"
      },
      {
        "anonymous": false,
        "inputs": [
          {
            "indexed": true,
            "internalType": "address",
            "name": "_from",
            "type": "address"
          },
          {
            "indexed": true,
            "internalType": "address",
            "name": "_to",
            "type": "address"
          },
          {
            "indexed": false,
            "internalType": "uint256",
            "name": "_amount",
            "type": "uint256"
          }
        ],
        "name": "Transfer",
        "type": "event"
      },
      {
        "anonymous": false,
        "inputs": [
          {
            "indexed": true,
            "internalType": "address",
            "name": "_to",
            "type": "address"
          },
          {
            "indexed": false,
            "internalType": "uint256",
            "name": "_amount",
            "type": "uint256"
          }
        ],
        "name": "Withdraw",
        "type": "event"
      },
      {
        "constant": false,
        "inputs": [
          {
            "internalType": "address",
            "name": "_newOwner",
            "type": "address"
          },
          {
            "internalType": "string",
            "name": "newOwnerName",
            "type": "string"
          }
        ],
        "name": "addOwner",
        "outputs": [],
        "payable": false,
        "stateMutability": "nonpayable",
        "type": "function"
      },
      {
        "constant": true,
        "inputs": [
          {
            "internalType": "address",
            "name": "_owner",
            "type": "address"
          },
          {
            "internalType": "address",
            "name": "_spender",
            "type": "address"
          }
        ],
        "name": "allowance",
        "outputs": [
          {
            "internalType": "uint256",
            "name": "",
            "type": "uint256"
          }
        ],
        "payable": false,
        "stateMutability": "view",
        "type": "function"
      },
      {
        "constant": false,
        "inputs": [
          {
            "internalType": "address",
            "name": "_spender",
            "type": "address"
          },
          {
            "internalType": "uint256",
            "name": "_amount",
            "type": "uint256"
          }
        ],
        "name": "approve",
        "outputs": [
          {
            "internalType": "bool",
            "name": "",
            "type": "bool"
          }
        ],
        "payable": false,
        "stateMutability": "nonpayable",
        "type": "function"
      },
      {
        "constant": true,
        "inputs": [
          {
            "internalType": "address",
            "name": "_addr",
            "type": "address"
          }
        ],
        "name": "balanceOf",
        "outputs": [
          {
            "internalType": "uint256",
            "name": "",
            "type": "uint256"
          }
        ],
        "payable": false,
        "stateMutability": "view",
        "type": "function"
      },
      {
        "constant": false,
        "inputs": [
          {
            "internalType": "address[]",
            "name": "_from",
            "type": "address[]"
          },
          {
            "internalType": "address[]",
            "name": "_to",
            "type": "address[]"
          },
          {
            "internalType": "uint256[]",
            "name": "_amount",
            "type": "uint256[]"
          }
        ],
        "name": "batchTransfer",
        "outputs": [],
        "payable": false,
        "stateMutability": "nonpayable",
        "type": "function"
      },
      {
        "constant": false,
        "inputs": [
          {
            "internalType": "address[]",
            "name": "_from",
            "type": "address[]"
          },
          {
            "internalType": "address[]",
            "name": "_to",
            "type": "address[]"
          },
          {
            "internalType": "uint256[]",
            "name": "_amount",
            "type": "uint256[]"
          },
          {
            "internalType": "uint256[]",
            "name": "_fee",
            "type": "uint256[]"
          }
        ],
        "name": "batchTransferWithFee",
        "outputs": [],
        "payable": false,
        "stateMutability": "nonpayable",
        "type": "function"
      },
      {
        "constant": false,
        "inputs": [
          {
            "internalType": "address",
            "name": "_from",
            "type": "address"
          },
          {
            "internalType": "uint256",
            "name": "_amount",
            "type": "uint256"
          }
        ],
        "name": "burnToken",
        "outputs": [],
        "payable": false,
        "stateMutability": "nonpayable",
        "type": "function"
      },
      {
        "constant": false,
        "inputs": [
          {
            "internalType": "string",
            "name": "_name",
            "type": "string"
          }
        ],
        "name": "changeName",
        "outputs": [],
        "payable": false,
        "stateMutability": "nonpayable",
        "type": "function"
      },
      {
        "constant": false,
        "inputs": [
          {
            "internalType": "string",
            "name": "_symbol",
            "type": "string"
          }
        ],
        "name": "changeSysbol",
        "outputs": [],
        "payable": false,
        "stateMutability": "nonpayable",
        "type": "function"
      },
      {
        "constant": true,
        "inputs": [],
        "name": "coldWallet",
        "outputs": [
          {
            "internalType": "address",
            "name": "",
            "type": "address"
          }
        ],
        "payable": false,
        "stateMutability": "view",
        "type": "function"
      },
      {
        "constant": true,
        "inputs": [],
        "name": "company",
        "outputs": [
          {
            "internalType": "string",
            "name": "",
            "type": "string"
          }
        ],
        "payable": false,
        "stateMutability": "view",
        "type": "function"
      },
      {
        "constant": true,
        "inputs": [],
        "name": "daiToken",
        "outputs": [
          {
            "internalType": "contract ERC20",
            "name": "",
            "type": "address"
          }
        ],
        "payable": false,
        "stateMutability": "view",
        "type": "function"
      },
      {
        "constant": true,
        "inputs": [],
        "name": "decimals",
        "outputs": [
          {
            "internalType": "uint8",
            "name": "",
            "type": "uint8"
          }
        ],
        "payable": false,
        "stateMutability": "view",
        "type": "function"
      },
      {
        "constant": false,
        "inputs": [
          {
            "internalType": "address",
            "name": "_from",
            "type": "address"
          },
          {
            "internalType": "uint256",
            "name": "amount",
            "type": "uint256"
          }
        ],
        "name": "deposit",
        "outputs": [
          {
            "internalType": "bool",
            "name": "",
            "type": "bool"
          }
        ],
        "payable": false,
        "stateMutability": "nonpayable",
        "type": "function"
      },
      {
        "constant": true,
        "inputs": [],
        "name": "depositDecimals",
        "outputs": [
          {
            "internalType": "uint8",
            "name": "",
            "type": "uint8"
          }
        ],
        "payable": false,
        "stateMutability": "view",
        "type": "function"
      },
      {
        "constant": false,
        "inputs": [
          {
            "internalType": "address",
            "name": "_from",
            "type": "address"
          },
          {
            "internalType": "address",
            "name": "_to",
            "type": "address"
          },
          {
            "internalType": "uint256",
            "name": "_amount",
            "type": "uint256"
          }
        ],
        "name": "intTransfer",
        "outputs": [
          {
            "internalType": "bool",
            "name": "",
            "type": "bool"
          }
        ],
        "payable": false,
        "stateMutability": "nonpayable",
        "type": "function"
      },
      {
        "constant": false,
        "inputs": [
          {
            "internalType": "address",
            "name": "_from",
            "type": "address"
          },
          {
            "internalType": "address",
            "name": "_to",
            "type": "address"
          },
          {
            "internalType": "uint256",
            "name": "_value",
            "type": "uint256"
          },
          {
            "internalType": "uint256",
            "name": "_fee",
            "type": "uint256"
          }
        ],
        "name": "intTransferWithFee",
        "outputs": [
          {
            "internalType": "bool",
            "name": "",
            "type": "bool"
          }
        ],
        "payable": false,
        "stateMutability": "nonpayable",
        "type": "function"
      },
      {
        "constant": true,
        "inputs": [
          {
            "internalType": "address",
            "name": "_owner",
            "type": "address"
          }
        ],
        "name": "isOwner",
        "outputs": [
          {
            "internalType": "bool",
            "name": "",
            "type": "bool"
          }
        ],
        "payable": false,
        "stateMutability": "view",
        "type": "function"
      },
      {
        "constant": false,
        "inputs": [
          {
            "internalType": "address",
            "name": "_to",
            "type": "address"
          },
          {
            "internalType": "uint256",
            "name": "_amount",
            "type": "uint256"
          }
        ],
        "name": "mintToken",
        "outputs": [],
        "payable": false,
        "stateMutability": "nonpayable",
        "type": "function"
      },
      {
        "constant": false,
        "inputs": [
          {
            "internalType": "address",
            "name": "_from",
            "type": "address"
          }
        ],
        "name": "moveBalanceFromOldVersion",
        "outputs": [
          {
            "internalType": "bool",
            "name": "",
            "type": "bool"
          }
        ],
        "payable": false,
        "stateMutability": "nonpayable",
        "type": "function"
      },
      {
        "constant": true,
        "inputs": [],
        "name": "name",
        "outputs": [
          {
            "internalType": "string",
            "name": "",
            "type": "string"
          }
        ],
        "payable": false,
        "stateMutability": "view",
        "type": "function"
      },
      {
        "constant": false,
        "inputs": [
          {
            "internalType": "address",
            "name": "_owner",
            "type": "address"
          }
        ],
        "name": "removeOwner",
        "outputs": [],
        "payable": false,
        "stateMutability": "nonpayable",
        "type": "function"
      },
      {
        "constant": false,
        "inputs": [
          {
            "internalType": "address",
            "name": "_newAddr",
            "type": "address"
          }
        ],
        "name": "setColdWallet",
        "outputs": [
          {
            "internalType": "bool",
            "name": "",
            "type": "bool"
          }
        ],
        "payable": false,
        "stateMutability": "nonpayable",
        "type": "function"
      },
      {
        "constant": false,
        "inputs": [],
        "name": "setStopAdmin",
        "outputs": [],
        "payable": false,
        "stateMutability": "nonpayable",
        "type": "function"
      },
      {
        "constant": false,
        "inputs": [
          {
            "internalType": "bool",
            "name": "_control",
            "type": "bool"
          }
        ],
        "name": "setStopControl",
        "outputs": [],
        "payable": false,
        "stateMutability": "nonpayable",
        "type": "function"
      },
      {
        "constant": true,
        "inputs": [],
        "name": "stopAdmin",
        "outputs": [
          {
            "internalType": "bool",
            "name": "",
            "type": "bool"
          }
        ],
        "payable": false,
        "stateMutability": "view",
        "type": "function"
      },
      {
        "constant": true,
        "inputs": [
          {
            "internalType": "address",
            "name": "",
            "type": "address"
          }
        ],
        "name": "stopControl",
        "outputs": [
          {
            "internalType": "bool",
            "name": "",
            "type": "bool"
          }
        ],
        "payable": false,
        "stateMutability": "view",
        "type": "function"
      },
      {
        "constant": true,
        "inputs": [],
        "name": "swdaiOldVersion",
        "outputs": [
          {
            "internalType": "contract ERC20",
            "name": "",
            "type": "address"
          }
        ],
        "payable": false,
        "stateMutability": "view",
        "type": "function"
      },
      {
        "constant": true,
        "inputs": [],
        "name": "symbol",
        "outputs": [
          {
            "internalType": "string",
            "name": "",
            "type": "string"
          }
        ],
        "payable": false,
        "stateMutability": "view",
        "type": "function"
      },
      {
        "constant": true,
        "inputs": [],
        "name": "totalSupply",
        "outputs": [
          {
            "internalType": "uint256",
            "name": "",
            "type": "uint256"
          }
        ],
        "payable": false,
        "stateMutability": "view",
        "type": "function"
      },
      {
        "constant": false,
        "inputs": [
          {
            "internalType": "address",
            "name": "_to",
            "type": "address"
          },
          {
            "internalType": "uint256",
            "name": "_amount",
            "type": "uint256"
          }
        ],
        "name": "transfer",
        "outputs": [
          {
            "internalType": "bool",
            "name": "",
            "type": "bool"
          }
        ],
        "payable": false,
        "stateMutability": "nonpayable",
        "type": "function"
      },
      {
        "constant": false,
        "inputs": [
          {
            "internalType": "address",
            "name": "_from",
            "type": "address"
          },
          {
            "internalType": "address",
            "name": "_to",
            "type": "address"
          },
          {
            "internalType": "uint256",
            "name": "_amount",
            "type": "uint256"
          }
        ],
        "name": "transferFrom",
        "outputs": [
          {
            "internalType": "bool",
            "name": "",
            "type": "bool"
          }
        ],
        "payable": false,
        "stateMutability": "nonpayable",
        "type": "function"
      },
      {
        "constant": true,
        "inputs": [],
        "name": "version",
        "outputs": [
          {
            "internalType": "uint8",
            "name": "",
            "type": "uint8"
          }
        ],
        "payable": false,
        "stateMutability": "view",
        "type": "function"
      },
      {
        "constant": false,
        "inputs": [
          {
            "internalType": "uint256",
            "name": "_amount",
            "type": "uint256"
          }
        ],
        "name": "withdraw",
        "outputs": [
          {
            "internalType": "bool",
            "name": "",
            "type": "bool"
          }
        ],
        "payable": false,
        "stateMutability": "nonpayable",
        "type": "function"
      },
      {
        "constant": false,
        "inputs": [
          {
            "internalType": "address",
            "name": "_to",
            "type": "address"
          },
          {
            "internalType": "uint256",
            "name": "_amount",
            "type": "uint256"
          }
        ],
        "name": "withdrawInternal",
        "outputs": [
          {
            "internalType": "bool",
            "name": "",
            "type": "bool"
          }
        ],
        "payable": false,
        "stateMutability": "nonpayable",
        "type": "function"
      },
      {
        "constant": false,
        "inputs": [
          {
            "internalType": "uint256",
            "name": "amount",
            "type": "uint256"
          },
          {
            "internalType": "address",
            "name": "_contract",
            "type": "address"
          },
          {
            "internalType": "address",
            "name": "_to",
            "type": "address"
          }
        ],
        "name": "withdrawStupidUser",
        "outputs": [],
        "payable": false,
        "stateMutability": "nonpayable",
        "type": "function"
      }
        ];

    var abiRepay = [{"inputs":[],"payable":false,"stateMutability":"nonpayable","type":"constructor"},{"constant":true,"inputs":[],"name":"CURRENCY","outputs":[{"internalType":"string","name":"","type":"string"}],"payable":false,"stateMutability":"view","type":"function"},{"constant":true,"inputs":[],"name":"SECPYEAR","outputs":[{"internalType":"uint256","name":"","type":"uint256"}],"payable":false,"stateMutability":"view","type":"function"},{"constant":true,"inputs":[{"internalType":"uint256","name":"_conID","type":"uint256"},{"internalType":"uint256","name":"_amount","type":"uint256"}],"name":"_testPaidContract","outputs":[{"internalType":"uint256","name":"principlePaid","type":"uint256"},{"internalType":"uint256","name":"sumInt","type":"uint256"},{"internalType":"uint256","name":"poolConid","type":"uint256"},{"internalType":"uint256","name":"data2","type":"uint256"},{"internalType":"uint256","name":"data0","type":"uint256"},{"internalType":"string","name":"_error","type":"string"}],"payable":false,"stateMutability":"view","type":"function"},{"constant":true,"inputs":[],"name":"admin","outputs":[{"internalType":"address","name":"","type":"address"}],"payable":false,"stateMutability":"view","type":"function"},{"constant":true,"inputs":[],"name":"catToken","outputs":[{"internalType":"contract CATToken","name":"","type":"address"}],"payable":false,"stateMutability":"view","type":"function"},{"constant":false,"inputs":[{"internalType":"address","name":"_newAdmin","type":"address"}],"name":"changeAdmin","outputs":[],"payable":false,"stateMutability":"nonpayable","type":"function"},{"constant":true,"inputs":[],"name":"contractDB","outputs":[{"internalType":"contract LoanContractDB","name":"","type":"address"}],"payable":false,"stateMutability":"view","type":"function"},{"constant":true,"inputs":[],"name":"decimal","outputs":[{"internalType":"uint256","name":"","type":"uint256"}],"payable":false,"stateMutability":"view","type":"function"},{"constant":true,"inputs":[{"internalType":"uint256","name":"_tokenID","type":"uint256"}],"name":"getTokenDetail","outputs":[{"internalType":"bool","name":"error","type":"bool"},{"internalType":"string","name":"message","type":"string"},{"internalType":"address","name":"_tokenAddr","type":"address"},{"internalType":"address","name":"_poolAddr","type":"address"},{"internalType":"uint256","name":"principle","type":"uint256"},{"internalType":"uint256","name":"sumInt","type":"uint256"},{"internalType":"uint256","name":"_loanConID","type":"uint256"},{"internalType":"uint256","name":"_status","type":"uint256"},{"internalType":"uint256","name":"_conID","type":"uint256"}],"payable":false,"stateMutability":"view","type":"function"},{"constant":false,"inputs":[{"internalType":"uint256","name":"_loanConID","type":"uint256"},{"internalType":"uint256","name":"_amount","type":"uint256"},{"internalType":"uint256","name":"_fee","type":"uint256"},{"internalType":"address","name":"_from","type":"address"},{"internalType":"address","name":"poolAddr","type":"address"},{"internalType":"uint256","name":"_tokenID","type":"uint256"},{"internalType":"uint256","name":"_conID","type":"uint256"}],"name":"paidTokenDefaultFee","outputs":[{"internalType":"bool","name":"","type":"bool"}],"payable":false,"stateMutability":"nonpayable","type":"function"},{"constant":true,"inputs":[],"name":"ratToken","outputs":[{"internalType":"contract RatToken","name":"","type":"address"}],"payable":false,"stateMutability":"view","type":"function"},{"constant":true,"inputs":[],"name":"sellSZO","outputs":[{"internalType":"contract SELLSZO","name":"","type":"address"}],"payable":false,"stateMutability":"view","type":"function"},{"constant":false,"inputs":[{"internalType":"address","name":"_addr","type":"address"}],"name":"setContractDB","outputs":[{"internalType":"bool","name":"","type":"bool"}],"payable":false,"stateMutability":"nonpayable","type":"function"},{"constant":false,"inputs":[{"internalType":"address","name":"_addr","type":"address"}],"name":"setSellSZO","outputs":[{"internalType":"bool","name":"","type":"bool"}],"payable":false,"stateMutability":"nonpayable","type":"function"},{"constant":true,"inputs":[],"name":"version","outputs":[{"internalType":"uint256","name":"","type":"uint256"}],"payable":false,"stateMutability":"view","type":"function"}]

    var abiSZTools = [{"inputs":[],"payable":false,"stateMutability":"nonpayable","type":"constructor"},{"anonymous":false,"inputs":[{"indexed":false,"internalType":"address","name":"_addr","type":"address"}],"name":"AddPermit","type":"event"},{"anonymous":false,"inputs":[{"indexed":false,"internalType":"address","name":"_addr","type":"address"}],"name":"RemovePermit","type":"event"},{"constant":false,"inputs":[{"internalType":"address","name":"_addr","type":"address"}],"name":"addAllowPool","outputs":[{"internalType":"bool","name":"","type":"bool"}],"payable":false,"stateMutability":"nonpayable","type":"function"},{"constant":false,"inputs":[{"internalType":"address","name":"_addr","type":"address"}],"name":"addAllowToken","outputs":[{"internalType":"bool","name":"","type":"bool"}],"payable":false,"stateMutability":"nonpayable","type":"function"},{"constant":false,"inputs":[{"internalType":"address","name":"_addr","type":"address"}],"name":"addPermit","outputs":[],"payable":false,"stateMutability":"nonpayable","type":"function"},{"constant":true,"inputs":[{"internalType":"uint256","name":"","type":"uint256"}],"name":"allowPools","outputs":[{"internalType":"address","name":"","type":"address"}],"payable":false,"stateMutability":"view","type":"function"},{"constant":true,"inputs":[{"internalType":"uint256","name":"","type":"uint256"}],"name":"allowTokens","outputs":[{"internalType":"address","name":"","type":"address"}],"payable":false,"stateMutability":"view","type":"function"},{"constant":true,"inputs":[{"internalType":"address","name":"_addr","type":"address"}],"name":"balanceOfSZ","outputs":[{"internalType":"uint256","name":"sumBalance","type":"uint256"},{"internalType":"uint256[]","name":"_szToken","type":"uint256[]"}],"payable":false,"stateMutability":"view","type":"function"},{"constant":true,"inputs":[{"internalType":"address","name":"_addr","type":"address"}],"name":"isPermit","outputs":[{"internalType":"bool","name":"","type":"bool"}],"payable":false,"stateMutability":"view","type":"function"},{"constant":true,"inputs":[{"internalType":"address","name":"","type":"address"}],"name":"permits","outputs":[{"internalType":"bool","name":"","type":"bool"}],"payable":false,"stateMutability":"view","type":"function"},{"constant":false,"inputs":[{"internalType":"address","name":"_addr","type":"address"}],"name":"removePermit","outputs":[],"payable":false,"stateMutability":"nonpayable","type":"function"},{"constant":false,"inputs":[{"internalType":"address","name":"_addr","type":"address"},{"internalType":"bool","name":"_set","type":"bool"}],"name":"setDisablePool","outputs":[{"internalType":"bool","name":"","type":"bool"}],"payable":false,"stateMutability":"nonpayable","type":"function"},{"constant":false,"inputs":[{"internalType":"address","name":"_addr","type":"address"},{"internalType":"bool","name":"_set","type":"bool"}],"name":"setDisableToken","outputs":[{"internalType":"bool","name":"","type":"bool"}],"payable":false,"stateMutability":"nonpayable","type":"function"},{"constant":true,"inputs":[],"name":"sumMintToken","outputs":[{"internalType":"uint256","name":"sumBalance","type":"uint256"},{"internalType":"uint256[]","name":"_szToken","type":"uint256[]"}],"payable":false,"stateMutability":"view","type":"function"},{"constant":true,"inputs":[{"internalType":"address","name":"_addr","type":"address"}],"name":"summaryAllPools","outputs":[{"internalType":"uint256","name":"allSum","type":"uint256"},{"internalType":"uint256[]","name":"_pool","type":"uint256[]"}],"payable":false,"stateMutability":"view","type":"function"},{"constant":true,"inputs":[],"name":"summaryAvaliable","outputs":[{"internalType":"uint256","name":"sum","type":"uint256"},{"internalType":"uint256[]","name":"_pool","type":"uint256[]"}],"payable":false,"stateMutability":"view","type":"function"},{"constant":true,"inputs":[{"internalType":"address","name":"_addr","type":"address"}],"name":"summaryDepositPool","outputs":[{"internalType":"uint256","name":"sumDeposti","type":"uint256"},{"internalType":"uint256[]","name":"_pool","type":"uint256[]"}],"payable":false,"stateMutability":"view","type":"function"},{"constant":true,"inputs":[{"internalType":"address","name":"_addr","type":"address"}],"name":"summaryInterest","outputs":[{"internalType":"uint256","name":"sumDeposti","type":"uint256"},{"internalType":"uint256[]","name":"_pool","type":"uint256[]"}],"payable":false,"stateMutability":"view","type":"function"},{"constant":true,"inputs":[{"internalType":"address","name":"_addr","type":"address"}],"name":"summarySZOReward","outputs":[{"internalType":"uint256","name":"sumBalance","type":"uint256"},{"internalType":"uint256[]","name":"_pool","type":"uint256[]"}],"payable":false,"stateMutability":"view","type":"function"},{"constant":true,"inputs":[],"name":"summaryTotalBorrow","outputs":[{"internalType":"uint256","name":"totalBorrow","type":"uint256"},{"internalType":"uint256[]","name":"_pool","type":"uint256[]"}],"payable":false,"stateMutability":"view","type":"function"},{"constant":true,"inputs":[],"name":"summaryTotalDeposit","outputs":[{"internalType":"uint256","name":"totalDeposit","type":"uint256"},{"internalType":"uint256[]","name":"_pool","type":"uint256[]"}],"payable":false,"stateMutability":"view","type":"function"},{"constant":true,"inputs":[],"name":"version","outputs":[{"internalType":"uint256","name":"","type":"uint256"}],"payable":false,"stateMutability":"view","type":"function"}]
    
    var abiRatToken = [{"inputs":[],"payable":false,"stateMutability":"nonpayable","type":"constructor"},{"anonymous":false,"inputs":[{"indexed":false,"internalType":"address","name":"_addr","type":"address"}],"name":"AddPermit","type":"event"},{"anonymous":false,"inputs":[{"indexed":true,"internalType":"address","name":"_addr","type":"address"},{"indexed":true,"internalType":"uint256","name":"_tokenID","type":"uint256"}],"name":"AllowBurn","type":"event"},{"anonymous":false,"inputs":[{"indexed":true,"internalType":"address","name":"owner","type":"address"},{"indexed":true,"internalType":"address","name":"approved","type":"address"},{"indexed":true,"internalType":"uint256","name":"tokenId","type":"uint256"}],"name":"Approval","type":"event"},{"anonymous":false,"inputs":[{"indexed":true,"internalType":"address","name":"owner","type":"address"},{"indexed":true,"internalType":"address","name":"operator","type":"address"},{"indexed":false,"internalType":"bool","name":"approved","type":"bool"}],"name":"ApprovalForAll","type":"event"},{"anonymous":false,"inputs":[{"indexed":true,"internalType":"address","name":"_addr","type":"address"},{"indexed":true,"internalType":"uint256","name":"_tokenID","type":"uint256"}],"name":"BurnToken","type":"event"},{"anonymous":false,"inputs":[{"indexed":true,"internalType":"address","name":"_newAdmin","type":"address"},{"indexed":true,"internalType":"address","name":"_oldAdmin","type":"address"}],"name":"ChangeAdmin","type":"event"},{"anonymous":false,"inputs":[{"indexed":true,"internalType":"address","name":"_addr","type":"address"},{"indexed":true,"internalType":"uint256","name":"_tokenID","type":"uint256"}],"name":"MintToken","type":"event"},{"anonymous":false,"inputs":[{"indexed":false,"internalType":"address","name":"_addr","type":"address"}],"name":"RemovePermit","type":"event"},{"anonymous":false,"inputs":[{"indexed":true,"internalType":"address","name":"_from","type":"address"},{"indexed":true,"internalType":"address","name":"to","type":"address"},{"indexed":true,"internalType":"uint256","name":"tokenId","type":"uint256"}],"name":"Transfer","type":"event"},{"constant":false,"inputs":[{"internalType":"address","name":"_addr","type":"address"}],"name":"addPermit","outputs":[],"payable":false,"stateMutability":"nonpayable","type":"function"},{"constant":true,"inputs":[],"name":"admin","outputs":[{"internalType":"address","name":"","type":"address"}],"payable":false,"stateMutability":"view","type":"function"},{"constant":true,"inputs":[],"name":"adminChangeKey","outputs":[{"internalType":"bytes32","name":"","type":"bytes32"}],"payable":false,"stateMutability":"view","type":"function"},{"constant":false,"inputs":[{"internalType":"uint256","name":"_tokenID","type":"uint256"}],"name":"allowBurn","outputs":[{"internalType":"bool","name":"","type":"bool"}],"payable":false,"stateMutability":"nonpayable","type":"function"},{"constant":false,"inputs":[{"internalType":"address","name":"to","type":"address"},{"internalType":"uint256","name":"tokenId","type":"uint256"}],"name":"approve","outputs":[],"payable":false,"stateMutability":"nonpayable","type":"function"},{"constant":true,"inputs":[{"internalType":"address","name":"owner","type":"address"}],"name":"balanceOf","outputs":[{"internalType":"uint256","name":"","type":"uint256"}],"payable":false,"stateMutability":"view","type":"function"},{"constant":true,"inputs":[],"name":"baseURI","outputs":[{"internalType":"string","name":"","type":"string"}],"payable":false,"stateMutability":"view","type":"function"},{"constant":false,"inputs":[{"internalType":"uint256","name":"_tokenID","type":"uint256"}],"name":"burnToken","outputs":[{"internalType":"bool","name":"","type":"bool"}],"payable":false,"stateMutability":"nonpayable","type":"function"},{"constant":true,"inputs":[{"internalType":"uint256","name":"","type":"uint256"}],"name":"canBurn","outputs":[{"internalType":"bool","name":"","type":"bool"}],"payable":false,"stateMutability":"view","type":"function"},{"constant":false,"inputs":[{"internalType":"address","name":"_newAdmin","type":"address"},{"internalType":"bytes32","name":"_keyData","type":"bytes32"},{"internalType":"bytes32[]","name":"merkleProof","type":"bytes32[]"},{"internalType":"bytes32","name":"_newRootKey","type":"bytes32"}],"name":"changeAdmin","outputs":[],"payable":false,"stateMutability":"nonpayable","type":"function"},{"constant":false,"inputs":[{"internalType":"string","name":"_name","type":"string"}],"name":"changeName","outputs":[],"payable":false,"stateMutability":"nonpayable","type":"function"},{"constant":false,"inputs":[{"internalType":"string","name":"_symbol","type":"string"}],"name":"changeSymbol","outputs":[],"payable":false,"stateMutability":"nonpayable","type":"function"},{"constant":false,"inputs":[{"internalType":"uint256","name":"_tokenID","type":"uint256"}],"name":"disableBurn","outputs":[{"internalType":"bool","name":"","type":"bool"}],"payable":false,"stateMutability":"nonpayable","type":"function"},{"constant":true,"inputs":[{"internalType":"uint256","name":"tokenId","type":"uint256"}],"name":"getApproved","outputs":[{"internalType":"address","name":"","type":"address"}],"payable":false,"stateMutability":"view","type":"function"},{"constant":true,"inputs":[{"internalType":"uint256","name":"_tokenID","type":"uint256"}],"name":"getRatDetail","outputs":[{"internalType":"uint256","name":"_tokenType","type":"uint256"},{"internalType":"uint256","name":"_docID","type":"uint256"},{"internalType":"address","name":"_contract","type":"address"}],"payable":false,"stateMutability":"view","type":"function"},{"constant":false,"inputs":[{"internalType":"address","name":"_from","type":"address"},{"internalType":"address","name":"_to","type":"address"},{"internalType":"uint256","name":"tokenId","type":"uint256"}],"name":"intTransfer","outputs":[{"internalType":"bool","name":"","type":"bool"}],"payable":false,"stateMutability":"nonpayable","type":"function"},{"constant":true,"inputs":[{"internalType":"address","name":"owner","type":"address"},{"internalType":"address","name":"operator","type":"address"}],"name":"isApprovedForAll","outputs":[{"internalType":"bool","name":"","type":"bool"}],"payable":false,"stateMutability":"view","type":"function"},{"constant":true,"inputs":[{"internalType":"address","name":"_addr","type":"address"}],"name":"isPermit","outputs":[{"internalType":"bool","name":"","type":"bool"}],"payable":false,"stateMutability":"view","type":"function"},{"constant":true,"inputs":[{"internalType":"uint256","name":"_tokeID","type":"uint256"}],"name":"isValidToken","outputs":[{"internalType":"bool","name":"","type":"bool"}],"payable":false,"stateMutability":"view","type":"function"},{"constant":false,"inputs":[{"internalType":"address","name":"_to","type":"address"},{"internalType":"uint256","name":"_tokenId","type":"uint256"},{"internalType":"uint256","name":"_docID","type":"uint256"},{"internalType":"uint256","name":"_tokenType","type":"uint256"},{"internalType":"address","name":"_contractAddr","type":"address"}],"name":"mintToken","outputs":[{"internalType":"bool","name":"","type":"bool"}],"payable":false,"stateMutability":"nonpayable","type":"function"},{"constant":true,"inputs":[],"name":"name","outputs":[{"internalType":"string","name":"","type":"string"}],"payable":false,"stateMutability":"view","type":"function"},{"constant":true,"inputs":[],"name":"newContractAddress","outputs":[{"internalType":"address","name":"","type":"address"}],"payable":false,"stateMutability":"view","type":"function"},{"constant":true,"inputs":[{"internalType":"uint256","name":"tokenId","type":"uint256"}],"name":"ownerOf","outputs":[{"internalType":"address","name":"","type":"address"}],"payable":false,"stateMutability":"view","type":"function"},{"constant":true,"inputs":[{"internalType":"address","name":"","type":"address"}],"name":"permits","outputs":[{"internalType":"bool","name":"","type":"bool"}],"payable":false,"stateMutability":"view","type":"function"},{"constant":false,"inputs":[{"internalType":"address","name":"_addr","type":"address"}],"name":"removePermit","outputs":[],"payable":false,"stateMutability":"nonpayable","type":"function"},{"constant":false,"inputs":[{"internalType":"address","name":"_from","type":"address"},{"internalType":"address","name":"to","type":"address"},{"internalType":"uint256","name":"tokenId","type":"uint256"}],"name":"safeTransferFrom","outputs":[],"payable":false,"stateMutability":"nonpayable","type":"function"},{"constant":false,"inputs":[{"internalType":"address","name":"_from","type":"address"},{"internalType":"address","name":"to","type":"address"},{"internalType":"uint256","name":"tokenId","type":"uint256"},{"internalType":"bytes","name":"_data","type":"bytes"}],"name":"safeTransferFrom","outputs":[],"payable":false,"stateMutability":"nonpayable","type":"function"},{"constant":false,"inputs":[{"internalType":"address","name":"operator","type":"address"},{"internalType":"bool","name":"approved","type":"bool"}],"name":"setApprovalForAll","outputs":[],"payable":false,"stateMutability":"nonpayable","type":"function"},{"constant":false,"inputs":[{"internalType":"string","name":"_baseURI_","type":"string"}],"name":"setBaseURI","outputs":[],"payable":false,"stateMutability":"nonpayable","type":"function"},{"constant":false,"inputs":[{"internalType":"address","name":"_addr","type":"address"}],"name":"setTools","outputs":[],"payable":false,"stateMutability":"nonpayable","type":"function"},{"constant":true,"inputs":[{"internalType":"bytes4","name":"interfaceId","type":"bytes4"}],"name":"supportsInterface","outputs":[{"internalType":"bool","name":"","type":"bool"}],"payable":false,"stateMutability":"view","type":"function"},{"constant":true,"inputs":[],"name":"symbol","outputs":[{"internalType":"string","name":"","type":"string"}],"payable":false,"stateMutability":"view","type":"function"},{"constant":true,"inputs":[{"internalType":"uint256","name":"index","type":"uint256"}],"name":"tokenByIndex","outputs":[{"internalType":"uint256","name":"","type":"uint256"}],"payable":false,"stateMutability":"view","type":"function"},{"constant":true,"inputs":[{"internalType":"address","name":"owner","type":"address"},{"internalType":"uint256","name":"index","type":"uint256"}],"name":"tokenOfOwnerByIndex","outputs":[{"internalType":"uint256","name":"","type":"uint256"}],"payable":false,"stateMutability":"view","type":"function"},{"constant":true,"inputs":[{"internalType":"uint256","name":"tokenId","type":"uint256"}],"name":"tokenURI","outputs":[{"internalType":"string","name":"","type":"string"}],"payable":false,"stateMutability":"view","type":"function"},{"constant":true,"inputs":[],"name":"totalSupply","outputs":[{"internalType":"uint256","name":"","type":"uint256"}],"payable":false,"stateMutability":"view","type":"function"},{"constant":false,"inputs":[{"internalType":"address","name":"_from","type":"address"},{"internalType":"address","name":"to","type":"address"},{"internalType":"uint256","name":"tokenId","type":"uint256"}],"name":"transferFrom","outputs":[],"payable":false,"stateMutability":"nonpayable","type":"function"},{"constant":true,"inputs":[{"internalType":"bytes32","name":"root","type":"bytes32"},{"internalType":"bytes32","name":"leaf","type":"bytes32"},{"internalType":"bytes32[]","name":"proof","type":"bytes32[]"}],"name":"verify","outputs":[{"internalType":"bool","name":"","type":"bool"}],"payable":false,"stateMutability":"pure","type":"function"},{"constant":true,"inputs":[],"name":"version","outputs":[{"internalType":"uint256","name":"","type":"uint256"}],"payable":false,"stateMutability":"view","type":"function"}]

    var poolDai;
    var poolUSDT;
    var poolUSDC;

    var szDai;
    var szUSDT;
    var szUSDC;

    var dai,usdc,usdt;

    var poolsSelect;
    var tokenSelect;
    var stableSelect;

    var contractPool,contractToken;

    var network;

    var szoRewardPools;


    var poolDAIAddr,poolUSDTAddr,poolUSDCAddr;
    var szTools;
    var szToolsAddr;
    var ratTokenAddr;
    var ratToken;
    var contract;
    var repayment;
    var repaymentAddr;

    var szDaiHave,szUSDTHave,szUSDCHave;


    function addStatusLine(text) {
      document.getElementById("status").innerHTML = text + "<br/><br/>" + document.getElementById("status").innerHTML;
    }


    async function getRATDetail(tokenID){
      return new Promise(function (resolve,reject){            
              ratToken.methods.getRatDetail(tokenID).call().then(function(result){
                    resolve(result);
               })

            })
    }

    async function getTokenDetail(tokenID){
        console.log(repayment);
            return new Promise(function (resolve,reject){            
              repayment.methods.getTokenDetail(tokenID).call().then(function(result){
                    resolve(result);
               })

            })
    }

    function httpGet(theUrl) {
      var xmlHttp = new XMLHttpRequest();
        xmlHttp.open("GET", theUrl, false); // false for synchronous request
        xmlHttp.send(null);
      return xmlHttp.responseText;
    }

    async function getGasUse() {
        var res = httpGet("https://ethgasstation.info/api/ethgasAPI.json");
        var json = JSON.parse(res);
        var gas = 0;

        var gasFast = json.fastest;// json.fast;//500;
        gasFast = gasFast / 10;

        gas = web3.utils.toWei(gasFast.toString(), 'Gwei');
        console.log("Cal Gas = " + gas);
        console.log(gas);
        return gas; // gasFast.toString();

    }

    async function checkContract(){
      let _tokenID = parseInt(tokenid);
      //let  ratDetail = await getRATDetail(_tokenID);
      //console.log(ratDetail);
      let tokenDetail = await getTokenDetail(_tokenID);
    //  console.log(tokenDetail);
      if(tokenDetail["error"] == true){
          alert("This ID "+_tokenID+" not Contract Token ");
          return;
      }

      document.getElementById("_tokentype").innerText = "Contract Token";
      document.getElementById("_principle").innerText = numberWithCommas(tokenDetail["principle"] / (10 ** decimal));
      document.getElementById("_interest").innerText = numberWithCommas(tokenDetail["sumInt"] / (10 ** decimal));

      payAmount = (tokenDetail["principle"] / (10 ** decimal)) + (tokenDetail["sumInt"] / (10 ** decimal));
      document.getElementById("repayment").value = payAmount;

      if(tokenDetail["_poolAddr"] == poolDAIAddr){
          document.getElementById("_szType").innerText = "szDAI";
      }
      else if(tokenDetail["_poolAddr"] == poolUSDTAddr){
          document.getElementById("_szType").innerText = "szUSDT";

      }
      else if(tokenDetail["_poolAddr"] == poolUSDCAddr){
          document.getElementById("_szType").innerText = "szUSDC";

      }
    }

    async function repaymentFunc(){
          let _tokenID = parseInt(tokenid);
          let _amountPay = parseInt(document.getElementById("repayment").value);

          if(!_tokenID){
            alert("Not put Token ID");
            return;
          }

          let tokenDetail = await getTokenDetail(_tokenID);
        //  console.log(tokenDetail);
          if(tokenDetail["error"] == true){
              alert("This ID "+_tokenID+" not Contract Token ");
              return;
          }

          if(tokenDetail["_poolAddr"] == poolDAIAddr){
            if(_amountPay > szDaiHave / (10 ** 18))
            {
              alert("Not enought SZDAI "+_amountPay+ "<" +(szDaiHave / (10**18)));
              return;
            }
          }
          else if(tokenDetail["_poolAddr"] == poolUSDTAddr){
            if(_amountPay > szUSDTHave / (10 ** 18))
            {
              alert("Not enought SZUSDT");
              return;
            }

          }
          else if(tokenDetail["_poolAddr"] == poolUSDCAddr){
            if(_amountPay > szUSDCHave / (10 ** 18))
            {
              alert("Not enought SZUSDC");
              return;
            }
          }


          let szoFee = web3.utils.toWei('50', 'ether');
          aGasPrice = await getGasUse();
          _amountPay = web3.utils.toWei(_amountPay.toString(), 'ether');



          repayment.methods.paidTokenDefaultFee(tokenDetail["_loanConID"],_amountPay,szoFee,ethAddress,tokenDetail["_poolAddr"],_tokenID,tokenDetail["_conID"]).send({from:ethAddress,gasPrice:aGasPrice,gas:400000})
                                .on('transactionHash', function(hash){
                                // alert("Done txHash= https://goerli.etherscan.io/tx/"+hash);

                                submitRepay(hash);

            })
              .on('error', function(error, receipt){
              alert("Error = "+error);
                           
                            //      process.exit();
            });

    }



    const ethEnabled = () => {
    if (window.ethereum) {
        window.web3 = new Web3(window.ethereum);
     
        return true;
      }
    return false;
    }


    if (!ethEnabled()) {
       alert("Please install an Ethereum-compatible browser or extension like MetaMask to use this dApp!");
    }




    async function getAccount(x){
       var accounts = await x.eth.getAccounts();
       var userAccount =accounts[0]
//      console.log(userAccount);
       return userAccount;
    }

    var decimal;
    var ethAddress;
    const web3 = window.web3;//new Web3(window.web3.currentProvider);
    function numberWithCommas(x) {

          var parts = x.toString().split(".");
          parts[0] = parts[0].replace(/\B(?=(\d{3})+(?!\d))/g, ",");
          return parts.join(".");
    }



    async function getETHAddr(){
        return new Promise(function (resolve,reject){            
               getAccount(web3).then(function(result){
                    resolve(result);
            })

        })
    }




    async function getSZBalance(){
       return new Promise(function (resolve,reject){   
                szTools.methods.balanceOfSZ(ethAddress).call().then(function(result){
                    resolve(result);
            })

        })
    }

    async function getBalance() {


        ethAddress = await getETHAddr();
        var szTotal = await getSZBalance();
        console.log(szTotal);


        szDaiHave = szTotal["_szToken"][0];
        szUSDTHave = szTotal["_szToken"][1];
        szUSDCHave = szTotal["_szToken"][2];

        document.getElementById("sumSZ").innerText = numberWithCommas(szTotal["sumBalance"] / (10 ** decimal));
        document.getElementById("szdaiBalance").innerText = numberWithCommas(szTotal["_szToken"][0] / (10 ** decimal));
        document.getElementById("szusdtBalance").innerText = numberWithCommas(szTotal["_szToken"][1] / (10 ** decimal));
        document.getElementById("szusdcBalance").innerText = numberWithCommas(szTotal["_szToken"][2] / (10 ** decimal));

        // document.getElementById("ethaddr").text = ethAddress;

        $("#ethaddr").text(ethAddress);


      addStatusLine("Current Balance = ");
    }


    async function getDecimal(){
       return new Promise(function (resolve,reject){
          DepositContract.methods.decimal().call().then(function(result){
              resolve(result);
          })

    })
          
    }

    async function getNetwork(){
      return new Promise(function (resolve,reject){
          web3.eth.net.getNetworkType().then(function(result){
          resolve(result);
        })
      })
    }

    async function getApproveStable(x,y,z){
        return new Promise(function (resolve,reject){
            x.methods.allowance(z,y).call().then(function(result){
                resolve(result);
            })
          })
    }

    async function mainRun(){
     // console.log("Call Decimal");
      await  window.ethereum.enable();
     // decimal = await getDecimal();
      decimal = 18;
      network = await getNetwork();
      ethAddress = await getETHAddr();



      console.log(network);
      if(network == 'main'){
          szToolsAddr = szToolsAddrMain;
          ratTokenAddr = ratTokenAddrMain;
          repaymentAddr = repaymentAddrMain;
          poolDAIAddr = daiPoolAddrMain;
          poolUSDTAddr = usdtPoolAddrMain;
          poolUSDCAddr = usdcPoolAddrMain;

          $("#btn_connect").hide();
          $("#ethaddr").show();

      }
      else if(network == 'kovan'){
          szToolsAddr = szToolsAddrKovan;
          ratTokenAddr = ratTokenAddrKovan;
          repaymentAddr = repaymentAddrKovan;
          poolDAIAddr = daiPoolAddrKovan;
          poolUSDTAddr = usdtPoolAddrKovan;
          poolUSDCAddr = usdcPoolAddrKovan;


      }else if(network == 'goerli'){
          szToolsAddr = szToolsAddrGoerli;
          ratTokenAddr = ratTokenAddrGoerli;
          repaymentAddr = repaymentAddrGoerli;
          poolDAIAddr = daiPoolAddrGoerli;
          poolUSDTAddr = usdtPoolAddrGoerli;
          poolUSDCAddr = usdcPoolAddrGoerli;
      }else {
        alert("Please check network");
        return;
      }

      szTools = new web3.eth.Contract(abiSZTools,szToolsAddr);
      ratToken = new web3.eth.Contract(abiRatToken,ratTokenAddr);
      repayment = new web3.eth.Contract(abiRepay,repaymentAddr);


      $("#ethNetwork").text(network);
      await getBalance();
      // document.getElementById("ethNetwork").text = network;
      checkContract();

    }

     // mainRun();

      function connectWallet() {
        mainRun();
        
      }

      $( document ).ready(function() {
        console.log( "ready!" );
        connectWallet();
      })

      $("#btn_connect").click(function() {
        connectWallet();
      });

      $("#btn_submit").click(function() {
        var a = $("#repayment").val();
        var token = $("#_szType").text();
        if (confirm("Are you sure for pay for " + a + " " + token + " ???"))
        {
          repaymentFunc();
        }
      });

      // $("#btn_test").click(function() {
      //   submitRepay("TEST");
      // })

      function submitRepay(hash) {

        if (hash != '') {
          $("#userid").val('999');
          $("#amount").val($("#repayment").val());
          $("#txhash").val(hash);
          // $("#loanid").val();

          console.log($("form").serializeArray());
          $("#frm").submit();
        }
      }
      

    </script>
    
  </body>
</html>