<? include '../include/superadmin_authen.php';?>
<?
$menuid = 1;
$userid = $_SESSION['userid'];
$status = $_REQUEST['status'];

$config = include ('../api/config.php');

$thisdate = date("m/d/Y");
$date1 = date("m/d/Y", strtotime(' -1 month'));
$dr = $_REQUEST['dr'];

if ($dr!='') {

  $date_range = explode("-", $dr);
  $currdr = $date_range;
  if (count($date_range)==2) {
    $date_range[0] = date_format(date_create(trim($date_range[0])),"Y-m-d");
    $date_range[1] = date_format(date_create(trim($date_range[1])),"Y-m-d");
  }

}else {
  $date_range[0] = date_format(date_create(trim($date1)),"Y-m-d");
  $date_range[1] = date_format(date_create(trim($thisdate)),"Y-m-d");

  $currdr[0] = $date1;
  $currdr[1] = $thisdate;
}

include '../include/database.php';
$db = new Database();  
$db->connect();

// $sql = "select * from loan_documents_v2 where complete_agreement=0 and date(createdate)>='$date_range[0]' and date(createdate)<='$date_range[1]'";

// $sql = "select a.*,ac.firstname,ac.lastname,ac.userid, ac.usertype as utype from (select * from loan_documents_v2 where complete_agreement=0 and status=0 and date(createdate)>='$date_range[0]' and date(createdate)<='$date_range[1]') a inner join loan_account ac on a.userid=ac.id order by a.createdate desc";

$sql = "select * from kyc_public where date(updatedate)>='$date_range[0]' and date(updatedate)<='$date_range[1]'";

if ($status == '')
  $status = 0;


$sql .= " and status=$status";

$db->sql($sql);
$res = $db->getResult();

?>
<!DOCTYPE html>
<html lang="en">

<? include '../include/head.php'; ?>

<style>
.modal {
    display:    none;
    position:   fixed;
    z-index:    1000;
    top:        0;
    left:       0;
    height:     100%;
    width:      100%;
    background: rgba( 255, 255, 255, .8 ) 
                url('http://i.stack.imgur.com/FhHRx.gif') 
                50% 50% 
                no-repeat;
}

/* When the body has the loading class, we turn
   the scrollbar off with overflow:hidden */
body.loading .modal {
    overflow: hidden;   
}

/* Anytime the body has the loading class, our
   modal element will be visible */
body.loading .modal {
    display: block;
}
</style>


  <body>
    <!-- Loader starts-->
    <? include '../include/loader.php'; ?>
    <!-- Loader ends-->
    <!-- page-wrapper Start-->
    <div class="page-wrapper">
      <!-- Page Header Start-->
      <? include '../include/top_bar.php'; ?>
      <!-- Page Header Ends -->
      <!-- Page Body Start-->
      <div class="page-body-wrapper">
        <!-- Page Sidebar Start-->
        <? include '../include/superadmin_left_bar.php'; ?>
        <!-- Page Sidebar Ends-->
        <!-- Right sidebar Start-->
        <? include '../include/right_bar.php'; ?>
        <!-- Right sidebar Ends-->
        <div class="page-body">
          <div class="container-fluid">
            
            <? include '../include/header_space.php';?>

              <div class="row">
                <div class="col">
                  <div class="card">
                  <div class="card-header">
                    <form action="" method="get" id="frm">
                    <div class="row">
                        <div class="col-md-2">
                          <h5>KYC List</h5>

                        </div>

                      

                        <div class="col-md-4">
                          <select class="form-control" name="status" onchange="update(this);">
                            <option value="0" <?if($status==0) echo " selected";?>>Submit</option>
                            <option value="1" <?if($status==1) echo " selected";?>>Active</option>
                            <option value="2" <?if($status==2) echo " selected";?>>Reject</option>
                          </select>
                        </div>

                        <div class="col-md-6">
                            <div class="row">
                              <div class="input-group col-md-7">
                                <input class="datepicker-here form-control digits" type="text" data-range="true" data-multiple-dates-separator=" - " data-language="en" value="<?=trim($currdr[0])?> - <?=trim($currdr[1])?>" name="dr">
                              </div>

                              <div class="input-group col-md-2">
                                <button type="submit" class="btn btn-secondary ">Search</button>
                              </div>
                            </div>
                        </div>
                    </div>
                    </form>
                  </div>
                  <div class="table-responsive">
                    <table class="table table-border-horizontal">
                      <thead>
                        <tr>
                          <th scope="col">Name</th>
                          <th scope="col">Lastname</th>
                          <th scope="col">Wallet Address</th>
                          <th scope="col">Birth Date</th>
                          <th scope="col">Doc</th>
                          <th scope="col">Country</th>
                          <th scope="col">Submit Date</th>
                          <!-- <th scope="col">Status</th> -->
                          <th scope="col">Action</th>
                        </tr>
                      </thead>
                      <tbody>
                      <? 
                      $hostname = getenv('HTTP_HOST');
                      $i=0;
                      foreach ($res as $r) { 
                        $show = "Submit";

                        if ($r['status'] == 1)
                          $show = "Active";
                    ?>
                        <tr>
                          <td><?=$r['name']?></td>
                          <td><?=$r['surname']?></td>
                          <td><?=$r['walletaddress']?></td>
                          <td><?=$r['birthdate']?></td>
                          <td><a href="http://<?=$hostname?>/szo/kyc/uploads/<?=$r['doc_image']?>" target="_blank">click</td>
                          <td><?=$r['countryname']?></td>
                          <td><?=$r['updatedate']?></td>
                          <!-- <td><?=$show?></td> -->
                          <td>
                            <a href="javascript:approve('<?=$r['id']?>')" class="btn btn-success btn-xs">Approve</a>

                            <a href="javascript:reject('<?=$r['id']?>')" class="btn btn-danger btn-xs">Reject</a>
                          </td>
                        </tr>
                      <? } ?>

                        
                      </tbody>
                    </table>
                  </div>
                </div>
                </div>
                
              </div>
              
          </div>
          
        </div>
        <div class="modal"><!-- Place at bottom of page --></div>
        <!-- footer start-->
        <? include '../include/footer.php'; ?>
      </div>
    </div>
    <!-- latest jquery-->
    <? include '../include/bottom_script.php'; ?>
    <!-- Plugin used-->
    <script>
      function update(t) {
        $("#frm").submit();
      }

      function approve(id) {
        call(id,1);
      }

      function reject(id) {
        call(id,2);
      }

      function call(id, st) {
        $.ajax({
          url: "kyc_approve.php",
          type: 'POST',
          data: {
            'kid' : id,
            'st' : st,
          },
          success: function (data) {
            // console.log(data);
            // alert(data);
            location.reload();
          }
        });
      }
    </script>
  </body>
</html>