<? include '../include/superadmin_authen.php';?>
<?
$menuid = 1;
$userid = $_SESSION['userid'];
$doc_path = "../assets/docs/widget";
$config = include ('../api/config.php');

$thisdate = date("m/d/Y", strtotime(' +1 day'));
$date1 = date("m/d/Y", strtotime(' -1 month'));
$dr = $_REQUEST['dr'];

if ($dr!='') {

  $date_range = explode("-", $dr);
  $currdr = $date_range;
  if (count($date_range)==2) {
    $date_range[0] = date_format(date_create(trim($date_range[0])),"Y-m-d");
    $date_range[1] = date_format(date_create(trim($date_range[1])),"Y-m-d");
  }

}else {
  $date_range[0] = date_format(date_create(trim($date1)),"Y-m-d");
  $date_range[1] = date_format(date_create(trim($thisdate)),"Y-m-d");

  $currdr[0] = $date1;
  $currdr[1] = $thisdate;
}

include '../include/database.php';
$db = new Database();  
$db->connect();

// $sql = "select * from loan_documents_v2 where complete_agreement=0 and date(createdate)>='$date_range[0]' and date(createdate)<='$date_range[1]'";

// $sql = "select a.*,ac.firstname,ac.lastname,ac.userid, ac.usertype as utype from (select * from loan_documents_v2 where complete_agreement=0 and (audit_approve=0 or legal_approve=0) and date(createdate)>='$date_range[0]' and date(createdate)<='$date_range[1]') a inner join loan_account ac on a.userid=ac.id order by a.createdate";

$sql = "select * from loan_widget_docs where date(updatedate)>='$date_range[0]' and date(updatedate)<='$date_range[1]' order by updatedate desc";


$db->sql($sql);
$res = $db->getResult();

?>
<!DOCTYPE html>
<html lang="en">

<? include '../include/head.php'; ?>

<style>
.modal {
    display:    none;
    position:   fixed;
    z-index:    1000;
    top:        0;
    left:       0;
    height:     100%;
    width:      100%;
    background: rgba( 255, 255, 255, .8 ) 
                url('http://i.stack.imgur.com/FhHRx.gif') 
                50% 50% 
                no-repeat;
}

/* When the body has the loading class, we turn
   the scrollbar off with overflow:hidden */
body.loading .modal {
    overflow: hidden;   
}

/* Anytime the body has the loading class, our
   modal element will be visible */
body.loading .modal {
    display: block;
}
</style>


  <body>
    <!-- Loader starts-->
    <? include '../include/loader.php'; ?>
    <!-- Loader ends-->
    <!-- page-wrapper Start-->
    <div class="page-wrapper">
      <!-- Page Header Start-->
      <? include '../include/top_bar.php'; ?>
      <!-- Page Header Ends -->
      <!-- Page Body Start-->
      <div class="page-body-wrapper">
        <!-- Page Sidebar Start-->
        <? include '../include/superadmin_left_bar.php'; ?>
        <!-- Page Sidebar Ends-->
        <!-- Right sidebar Start-->
        <? include '../include/right_bar.php'; ?>
        <!-- Right sidebar Ends-->
        <div class="page-body">
          <div class="container-fluid">
            
            <? include '../include/header_space.php';?>

              <div class="row">
                <div class="col">
                  <div class="card">
                  <div class="card-header">
                    <div class="row">
                      <div class="col-md-6">
                        <h5>Widget Applications</h5>
                      </div>

                      <div class="col-md-6">
                        <form action="" method="get">
                          <div class="row">

                            <div class="input-group col-md-7">
                              <input class="datepicker-here form-control digits" type="text" data-range="true" data-multiple-dates-separator=" - " data-language="en" value="<?=trim($currdr[0])?> - <?=trim($currdr[1])?>" name="dr">
                            </div>

                            <div class="input-group col-md-2">
                              <button type="submit" class="btn btn-secondary ">Search</button>
                            </div>
                          </div>
                        </form>
                      </div>
                    </div>

                  </div>
                  <div class="table-responsive">
                    <table class="table table-border-horizontal">
                      <thead>
                        <tr>
                          <th scope="col">#</th>
                          <th scope="col">Submit Date</th>
                          <!-- <th scope="col">File1</th>
                          <th scope="col">File2</th> -->
                          <th scope="col">Month 1</th>
                          <th scope="col">Month 2</th>
                          <th scope="col">Score</th>
                          <th scope="col">Max Borrow</th>
                          <th scope="col">Apr</th>
                          <th scope="col">Score Date</th>
                          <th scope="col">Action</th>
                        </tr>
                      </thead>
                      <tbody>
                      <? 

                      $i=0;
                      foreach ($res as $r) { 
                        $i ++;
                        $status = $r['status'];
                        if ($status == 0) {

                        }
                      ?>
                        <tr>
                          <th scope="row"><?=$i?></th>
                          <td><?=$r['updatedate']?></td>
                          <!-- <td><a href="<?=$doc_path?>/<?=$r['file1']?>" target="_blank"><?=$r['file1']?></a></td>
                          <td><a href="<?=$doc_path?>/<?=$r['file2']?>" target="_blank"><?=$r['file2']?></a></td> -->
                          <td><a href="<?=$doc_path?>/<?=$r['file3']?>" target="_blank"><?=$r['file3']?></a></td>
                          <td><a href="<?=$doc_path?>/<?=$r['file4']?>" target="_blank"><?=$r['file4']?></a></td>
                          <td><?=$r['score']?></td>
                          <td><?=$r['max_borrow']?></td>
                          <td><?=$r['apr']?></td>
                          <td><?=$r['scoredate']?></td>
                          <td>
                            <? if ($status==0) { ?>
                              <a href="loan_widget_score.php?id=<?=$r['id']?>">Update Score</a>
                            <? } ?>

                            <? if ($status==1) { ?>
                              <a href="loan_widget_score.php?id=<?=$r['id']?>&v=view">View</a>
                            <? } ?>
                          </td>
                        </tr>
                      <? } ?>

                        
                      </tbody>
                    </table>
                  </div>
                </div>
                </div>
                
              </div>
              
          </div>
          
        </div>
        <div class="modal"><!-- Place at bottom of page --></div>
        <!-- footer start-->
        <? include '../include/footer.php'; ?>
      </div>
    </div>
    <!-- latest jquery-->
    <? include '../include/bottom_script.php'; ?>
    <!-- Plugin used-->
    
  </body>
</html>