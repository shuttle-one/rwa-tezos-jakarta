<? include '../include/superadmin_authen.php';?>
<?
$menuid = 1;
$userid = $_SESSION['userid'];
$config = include ('../api/config.php');

$thisdate = date("m/d/Y");
$date1 = date("m/d/Y", strtotime(' -5 month'));
$dr = $_REQUEST['dr'];

if ($dr!='') {

  $date_range = explode("-", $dr);
  $currdr = $date_range;
  if (count($date_range)==2) {
    $date_range[0] = date_format(date_create(trim($date_range[0])),"Y-m-d");
    $date_range[1] = date_format(date_create(trim($date_range[1])),"Y-m-d");
  }

}else {
  $date_range[0] = date_format(date_create(trim($date1)),"Y-m-d");
  $date_range[1] = date_format(date_create(trim($thisdate)),"Y-m-d");

  $currdr[0] = $date1;
  $currdr[1] = $thisdate;
}

include '../include/database.php';
$db = new Database();  
$db->connect();

// $sql = "select * from loan_documents_v2 where complete_agreement=0 and date(createdate)>='$date_range[0]' and date(createdate)<='$date_range[1]'";

// $sql = "select a.*,ac.firstname,ac.lastname,ac.userid, ac.usertype as utype from (select * from loan_documents_v2 where status=7 and date(createdate)>='$date_range[0]' and date(createdate)<='$date_range[1]') a inner join loan_account ac on a.userid=ac.id";

$sql = "SELECT t.tokenid, d.title, d.payperiod, DATE_ADD(c.active_date, INTERVAL d.payperiod DAY) as expiredate,c.* FROM `loan_contract_v2` c left join loan_documents_v2 d on c.documentid=d.id left join loan_token t on c.id=t.loanid where date(c.active_date) BETWEEN '$date_range[0]' and '$date_range[1]' and DATE_ADD(c.active_date, INTERVAL d.payperiod DAY) < date(now()) ";

if ($config['TEST']==1)
      $sql .= " and c.test=1 ";
    else $sql .= " and c.test=0 ";
$sql .= " order by c.updatedate";

$db->sql($sql);
$res = $db->getResult();

?>
<!DOCTYPE html>
<html lang="en">

<? include '../include/head.php'; ?>

<style>
.modal {
    display:    none;
    position:   fixed;
    z-index:    1000;
    top:        0;
    left:       0;
    height:     100%;
    width:      100%;
    background: rgba( 255, 255, 255, .8 ) 
                url('http://i.stack.imgur.com/FhHRx.gif') 
                50% 50% 
                no-repeat;
}

/* When the body has the loading class, we turn
   the scrollbar off with overflow:hidden */
body.loading .modal {
    overflow: hidden;   
}

/* Anytime the body has the loading class, our
   modal element will be visible */
body.loading .modal {
    display: block;
}
</style>


  <body>
    <!-- Loader starts-->
    <? include '../include/loader.php'; ?>
    <!-- Loader ends-->
    <!-- page-wrapper Start-->
    <div class="page-wrapper">
      <!-- Page Header Start-->
      <? include '../include/top_bar.php'; ?>
      <!-- Page Header Ends -->
      <!-- Page Body Start-->
      <div class="page-body-wrapper">
        <!-- Page Sidebar Start-->
        <? include '../include/superadmin_left_bar.php'; ?>
        <!-- Page Sidebar Ends-->
        <!-- Right sidebar Start-->
        <? include '../include/right_bar.php'; ?>
        <!-- Right sidebar Ends-->
        <div class="page-body">
          <div class="container-fluid">
            
            <? include '../include/header_space.php';?>

              <div class="row">
                <div class="col">
                  <div class="card">
                  <div class="card-header">
                    <div class="row">
                      <div class="col-md-6">
                        <h5 class="danger">Loan Applications Expired</h5>
                        <!-- <br>
                        <?=$sql?> -->
                      </div>

                      <div class="col-md-6">
                        <form action="" method="get">
                          <div class="row">

                            <div class="input-group col-md-7">
                              <input class="datepicker-here form-control digits" type="text" data-range="true" data-multiple-dates-separator=" - " data-language="en" value="<?=trim($currdr[0])?> - <?=trim($currdr[1])?>" name="dr">
                            </div>

                            <div class="input-group col-md-2">
                              <button type="submit" class="btn btn-secondary ">Search</button>
                            </div>
                          </div>
                        </form>
                      </div>
                    </div>

                  </div>
                  <div class="table-responsive">
                    <table class="table table-border-horizontal">
                      <thead>
                        <tr>
                          <!-- <th scope="col">#</th> -->
                          <th scope="col">TokenID</th>
                          <th scope="col">Title</th>
                          <th scope="col">Amount</th>
                          <th scope="col">Principle</th>
                          <th scope="col">Must Pay</th>
                          <th scope="col">Interest</th>
                          <th scope="col">ActiveDate</th>
                          <th scope="col">ExpiredDate</th>
                          <th scope="col">txHash</th>
                          <th scope="col">Action</th>
                        </tr>
                      </thead>
                      <tbody>
                      <? 

                      $i=0;
                      foreach ($res as $r) { 
                          $i ++;
                          $spay = $r['principle'] + $r['interest'];
                      ?>
                        <tr>
                          <!-- <th scope="row"><?=$i?></th> -->
                          <td><?=$r['tokenid']?></td>
                          <td><?=$r['title']?></td>
                          <td><?=number_format($r['amount'], 4, '.', ',')?></td>
                          <td><?=number_format($r['principle'], 4, '.', ',')?></td>
                          <td><?=number_format($spay, 4, '.', ',')?></td>
                          <td><?=number_format($r['interest'], 4, '.', ',')?></td>
                          <td><?=$r['active_date']?></td>
                          <td><?=$r['expiredate']?></td>
                          <td>
                            <? if ($r['active_txhash']!='') { ?>
                            <a href="https://etherscan.io/tx/<?=$r['active_txhash']?>" target="_blank">
                              <?=substr($r['active_txhash'],0,10)?> .. 
                            </a>
                            <? } ?>
                          </td>
                          <td>
                            
                            <a href="loan_repay.php?id=<?=$r['tokenid']?>" class="btn btn-sm btn-primary"> Repay </a>
                          </td>
                        </tr>
                      <? } ?>

                        
                      </tbody>
                    </table>
                  </div>
                </div>
                </div>
                
              </div>
              
          </div>
          
        </div>
        <div class="modal"><!-- Place at bottom of page --></div>
        <!-- footer start-->
        <? include '../include/footer.php'; ?>
      </div>
    </div>
    <!-- latest jquery-->
    <? include '../include/bottom_script.php'; ?>
    <!-- Plugin used-->
    <script>


    </script>
    
  </body>
</html>