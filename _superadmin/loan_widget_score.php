<? include '../include/superadmin_authen.php';?>
<?

include '../include/database.php';
$db = new Database();  
$db->connect();

$id = $_REQUEST['id'];
$v = $_REQUEST['v'];
$readonly = '';

if ($v!='') 
  $readonly = ' readonly';

$sql = "select * from loan_widget_docs where id=$id";
$db->sql($sql);
$res = $db->getResult();

?>
<!DOCTYPE html>
<html lang="en">
  <? include '../include/head.php'; ?>
  <style>
.modal {
    display:    none;
    position:   fixed;
    z-index:    1000;
    top:        0;
    left:       0;
    height:     100%;
    width:      100%;
    background: rgba( 255, 255, 255, .8 ) 
                url('http://i.stack.imgur.com/FhHRx.gif') 
                50% 50% 
                no-repeat;
}

/* When the body has the loading class, we turn
   the scrollbar off with overflow:hidden */
body.loading .modal {
    overflow: hidden;   
}

/* Anytime the body has the loading class, our
   modal element will be visible */
body.loading .modal {
    display: block;
}
</style>
  <body>
    <!-- Loader starts-->
    <? include '../include/loader.php'; ?>
    <!-- Loader ends-->
    <!-- page-wrapper Start-->
    <div class="page-wrapper">
      <!-- Page Header Start-->
      <? include '../include/top_bar.php'; ?>
      <!-- Page Header Ends -->
      <!-- Page Body Start-->
      <div class="page-body-wrapper">
        <!-- Page Sidebar Start-->
        <? include '../include/superadmin_left_bar.php'; ?>
        <!-- Page Sidebar Ends-->
        <!-- Right sidebar Start-->
        <? include '../include/right_bar.php'; ?>
        <!-- Right sidebar Ends-->
        <div class="page-body">

          <? include '../include/header_space.php';?>

          <div class="container-fluid">
            <div class="row">

              <div class="col-sm-6">
                <div class="card">
                  <div class="card-header">
                    <h5>Super Admin Score Approve</h5>
                  </div>

                    <div class="card-body">
                      <form action="loan_widget_update.php" method="post">
                        <input type="hidden" name="id" value="<?=$id?>">
                        <div class="row form-group">
                          <div class="col-sm-12 form-group">
                            <label>Score</label>
                            <input class="form-control" name="score" id="score" type="text" placeholder="" value="<?=$res[0]['score']?>" <?=$readonly?>>
                          </div>

                          <div class="col-sm-12 form-group">
                            <label>Debt</label>
                            <input class="form-control" name="debt" id="debt" type="text" placeholder="" value="<?=$res[0]['max_borrow']?>" <?=$readonly?>>
                          </div>

                          <div class="col-sm-12 form-group">
                            <label>Apr/Year</label>
                            <input class="form-control" name="apr" id="apr" type="text" placeholder="" value="<?=$res[0]['apr']?>" <?=$readonly?>>
                          </div>

                        </div>

                        <div class="row">
                          <? if ($v=='')  { ?>
                          <div class="col-sm-6 form-group">
                            <button type="submit" id="btn_submit" class="btn btn-success">Submit</button>
                          </div>
                          <? } ?>


                          <div class="col-sm-6 form-group">
                            <button type="button" id="btn_back" class="btn btn-secondary">Back</button>
                          </div>

                        </div>
                      </form>
                      
                    </div>


                </div>
              </div>

              <div class="col-sm-6">
              <? if (count($res)==1) { ?>

                <div class="card">
                      <div class="card-header">
                        <h5>Widget submit documents</h5>
                        <!-- <span>Using the <a href="#">card</a> component, you can extend the default collapse behavior to create an accordion.</span> -->
                      </div>
                      <div class="card-body">
                        
                          <div class="form-group row">
                            <label class="col-sm-3 col-form-label" for="file1">Banker's Guarantee
                              </label>
                            <div class="col-sm-9">
                              <?if ($res[0]['file1']!='') { ?>
                                <a href="../assets/docs/widget/<?=$res[0]['file1']?>" target="_blank"><img src="../assets/docs/widget/<?=$res[0]['file1']?>" width="100"></a>
                              <? } ?>
                            </div>
                          </div>

                          <div class="form-group row">
                            <label class="col-sm-3 col-form-label" for="file2">Trade Invoice</label>
                            <div class="col-sm-9">
                              <?if ($res[0]['file2']!='') { ?>
                                <a href="../assets/docs/widget/<?=$res[0]['file2']?>" target="_blank"><img src="../assets/docs/widget/<?=$res[0]['file2']?>" width="100"></a>
                              <? } ?>
                            </div>
                          </div>

                          <div class="form-group row">
                            <label class="col-sm-3 col-form-label" for="file3">Month 1 Bank Statement</label>
                            <div class="col-sm-9">
                              <?if ($res[0]['file3']!='') { ?>
                                <? 
                                  $ar = explode('.',$res[0]['file3']);
                                  $ex = $ar[count($ar)-1];

                                  if (strtolower($ex)=='pdf') {
                                ?>
                                <a href="../assets/docs/widget/<?=$res[0]['file3']?>" target="_blank"><i class="fa fa-file-pdf-o"></i> Click to open attatched file</a>
                                <br>
                                <embed src="../assets/docs/widget/<?=$res[0]['file3']?>" type="application/pdf" width="200px" height="200px" />
                                <? 
                                  } else if (strtolower($ex)=='jpg' || strtolower($ex)=='jpeg' || strtolower($ex)=='gif' || strtolower($ex)=='png') { // IMAGE
                                  ?>
                                  <a href="../assets/docs/widget/<?=$res[0]['file3']?>" target="_blank"><i class="fa fa-file-image-o"></i> Click to open attatched file</a>
                                  <br>
                                  <img src="../assets/docs/widget/<?=$res[0]['file3']?>" width="200px">
                                  <?
                                  }else if (strtolower($ex)=='xls' || strtolower($ex)=='xlsx') {
                                    ?>
                                    <a href="../assets/docs/widget/<?=$res[0]['file3']?>" target="_blank"><i class="fa fa-file-excel-o"></i> Click to open attatched file</a>
                                    <?
                                  }else if (strtolower($ex)=='doc' || strtolower($ex)=='docs') {
                                    ?>
                                    <a href="../assets/docs/widget/<?=$res[0]['file3']?>" target="_blank"><i class="fa fa-file-word-o"></i> Click to open attatched file</a>
                                    <?
                                  }
                                } 
                                ?>
                            </div>
                          </div>

                          <div class="form-group row">
                            <label class="col-sm-3 col-form-label" for="file4">Month 2 Bank Statement</label>
                            <div class="col-sm-9">
                              <?if ($res[0]['file4']!='') { ?>

                                <!-- <a href="../assets/docs/<?=$res[0]['file4']?>" target="_blank">Click to show</a>
                                <br> -->

                                <? 
                                  $ar = explode('.',$res[0]['file4']);
                                  $ex = $ar[count($ar)-1];

                                  if (strtolower($ex)=='pdf') {
                                ?>
                                <a href="../assets/docs/widget/<?=$res[0]['file4']?>" target="_blank"><i class="fa fa-file-pdf-o"></i> Click to open attatched file</a>
                                <br>
                                <embed src="../assets/docs/widget/<?=$res[0]['file4']?>" type="application/pdf" width="200px" height="200px" />

                                <? 
                                  } else if (strtolower($ex)=='jpg' || strtolower($ex)=='jpeg' || strtolower($ex)=='gif' || strtolower($ex)=='png') { // IMAGE
                                  ?>
                                  <a href="../assets/docs/widget/<?=$res[0]['file4']?>" target="_blank"><i class="fa fa-file-image-o"></i> Click to open attatched file</a>
                                  <br>
                                  <img src="../assets/docs/widget/<?=$res[0]['file4']?>" width="200px">
                                  <?
                                  }else if (strtolower($ex)=='xls' || strtolower($ex)=='xlsx') {
                                    ?>
                                    <a href="../assets/docs/widget/<?=$res[0]['file4']?>" target="_blank"><i class="fa fa-file-excel-o"></i> Click to open attatched file</a>
                                    <?
                                  }else if (strtolower($ex)=='doc' || strtolower($ex)=='docs') {
                                    ?>
                                    <a href="../assets/docs/widget/<?=$res[0]['file4']?>" target="_blank"><i class="fa fa-file-word-o"></i> Click to open attatched file</a>
                                    <?
                                  }
                                } 
                                ?>
                            </div>
                          </div>
                      </div>

                    </div>
                    </form>
                  <? } else { echo "Not found this loan application."; }?>
              </div> 
            </div>
          </div>
          
        </div>
        <!-- footer start-->
        <? include '../include/footer.php'; ?>
        <div class="modal"><!-- Place at bottom of page --></div>

      </div>
    </div>
    <!-- latest jquery-->
    <? include '../include/bottom_script.php'; ?>
    <!-- Plugin used-->.
    <script>


    $("#btn_back").click(function() {
      window.location.href = 'loan_widget.php';
      // window.history.back();
    });

    </script>
  </body>
</html>