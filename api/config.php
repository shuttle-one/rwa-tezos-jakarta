<?

return array(
	"ADMIN_EMAIL" => "zhuang@shuttle.one",
	"ROOT_PATH" => "http://shuttleone.network/gets_v2/",
	"CONTEXT" => "http://natee.network:9005",
	// "CONTEXT_TEZOS" => "http://localhost:3000/",
	"CONTEXT_TEZOS" => "http://localhost:3003/",

	"TEST" => 1,
	"PLATFORM" => "web",
	
	// "CONTEXT" => "https://natee.network:9005",
	"CREATE_DOC" => "/loanV2/create_doc/11", 
	//loan/create_doc/(ETHADDR)/(TOKENID)/(DOCID)/(DOCTYPE)/test
	"CHECK_DOCIC" => "/loan/check_docid/11",
	//loan/check_docid/(DOCID)/test
	"STATUS_DOC" => "/loan/status_doc/11",
	//loan/status_doc/DOCID/test
	"LEGAL_OK" => "/loanV2/legal_doc/11",
	//loan/legalok/(ETHADDR)/(DOCID)/test
	"AUDIT_OK" => "/loanV2/audit_doc/11",
	//loan/auditok/(ETHADDR)/(DOCID)/(CREDIT)/test
	"LEGAL_KYC" => "/loan/legalkyc/11",
	///loan/legalkyc/(ETHADDR)/(LEGALID)/test
	"AUDIT_KYC" => "/loan/auditkyc/11",
	//loan/auditkyc/(ETHADDR)/(AUDITID)/test

	// "CREATE_LOAN" => "/loanV2/create_contract/",
	//loan/create_loan/(ETHADDR)/(TOKENID)/(DOCID)/(CONTRACTID)/(LOAN_AMOUNT)/(CURRENCY)/(EXRATE)/test
	"CHECK_DEBIT" => "/loan/checkdebit/",
	//loan/checkdebit/(CONTRACTID)

	"CONTRACT_DETAIL" => "/loanV2/contract_info/",
	//loanV2/contract_info/(SectionID)/(TOKENID)/test

	"CONTRACT_ACTIVE" => "/loanV2/activecontract/",


	"CHECK_INTERST" => "/loanV2/interest/", ///loanV2/interest/sectionid/contracti",



	//******** WALLET SECTION *******//

	"LOGIN" => "/acc/login/",//acc/login/username/pass//web/version/0/
	"GET_WALLET_WITH_SECTIONID" => "/acc/loginSection/",
	"USER_TRANSACTION" => "/wallet/transec/",

	"RECOVERY_PASSWORD" => "/acc/forgot/",  ///acc/forgot/email/test
	"USER_DETAIL" => "/acc/detail/",


	//******** TOP UP ***************//
	"TOPUP_COUNTRY" => "/wallet/topup_country/",
	"TOPUP_RATE" => "/wallet/topup_rate/",
	"TOPUP_BANK_DETAIL" => "/wallet/topup_bankdetail/",
	"TOPUP_BANK_LIST" => "/wallet/topup_banklist/",
	"TOPUP_CONFIRM" => "/wallet/topup_bankcon/",

	"TOPUP_BANK_CHECK" => "/wallet/topup_bankcheck/",
	"TOPUP_BANK_SUBMIT" => "/wallet/topup_banksubmit/",
	"TOPUP_DOC_UOLOAD_URL" => "https://shuttleone.network/xse_wallet/doc/upload_put_web.php",

	// "TOPUP_BUY_LIST" => "/wallet/outwallet/",
	"TOPUP_CHECK_PRICE" => "/buy/checkPrice/", ///buy/checkPrice/1594372229394/ETH/0.01/test
	"TOPUP_CONFIRM_RESERVE" => "/buy/confirm/",  // sectionid/resev
	"TOPUP_CURRENCY" => "GET",


	//******** REMITAMCE ***************//
	"WITHDRAW_COUNTRY" => "/wallet/withdraw_country/", //wallet/withdraw_country/1595224119123/
	"WITHDRAW_RATE" => "/wallet/withdraw_rate/", ///wallet/withdraw_rate/1595224119123/70/19
	"WITHDRAW_BKK" => "/wallet/withdraw_bkk/", //wallet/withdraw_bkk/1595229096220/สำเร็จ วจนะเสถียร/2222/4414/thai/10/19/110/test


	//******* REPAY ********************//
	"PAYBACK" => "/loanV2/payback_contract/",
	
	"NEXT_PERIOD" => "/loanV2/nextpay/", ///loanV2/nextpay/sectionid/contracti

	"KYC_CHECK" => "/szo/checkkyc/", ///szo/checkkyc/(ETH ADDRESS

	// "ETHER_SCAN" => "https://etherscan.io/tx/",
	"ETHER_SCAN" => "https://jakarta.tzstats.com/",
	);

?>