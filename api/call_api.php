<? require_once ('../api/function.php'); ?>
<? 

include '../include/database.php';
$db = new Database();  
$db->connect();

$command = $_REQUEST['command'];

if ($command=='topup_rate') {
	$sectionid = $_REQUEST['sectionid'];
	$amount = $_REQUEST['amount'];
	$ccode = $_REQUEST['ccode'];

	echo convertTopupAmount($sectionid, $ccode, $amount);
}
else if ($command=='topup_bank_detail') {
	$sectionid = $_REQUEST['sectionid'];
	$amount = $_REQUEST['amount'];
	$ccode = $_REQUEST['ccode'];

	echo topupBankDetail($sectionid, $ccode, $amount);
}else if ($command=='topup_confirm') {
	$id = $_REQUEST['id'];
	$sectionid = $_REQUEST['sectionid'];
	echo topupConfirm($sectionid, $id);
}else if ($command=='topup_bank_submit') {

	$id = $_REQUEST['id'];
	$sectionid = $_REQUEST['sectionid'];
	echo topupBankSubmit($sectionid, $id);
}else if ($command=='topup_check_price') {
	
	$sectionid = $_REQUEST['sectionid'];
	$amount = $_REQUEST['amount'];
	$token = $_REQUEST['token'];
	echo topupCheckPrice($sectionid, $amount, $token);
}else if ($command=='topup_reserve') {
	
	$sectionid = $_REQUEST['sectionid'];
	$resv = $_REQUEST['resv'];
	echo topupReserve($sectionid, $resv);
}else if ($command=='recovery_password') {
	$email = $_REQUEST['email'];

	echo recoeryPassword($email);
}else if ($command=='withdraw_country') {
	$sectionid = $_REQUEST['sectionid'];
	echo withdrawCountry($sectionid);
}else if ($command=='withdraw_rate') {
	$sectionid = $_REQUEST['sectionid'];
	$amount = $_REQUEST['amount'];
	$country = $_REQUEST['country'];
	echo withdrawRate($sectionid, $amount, $country);
}else if ($command=='repay') {
	$sectionid = $_REQUEST['sectionid'];
	$amount = $_REQUEST['amount'];
	$loanid = $_REQUEST['loanid'];
	$userid = $_REQUEST['userid'];
	$test = $_REQUEST['test'];

	$sql = "select * from loan_contract_v2 where id=$loanid";
	$db->sql($sql);
	$res = $db->getResult();


	if (count($res)==1) {
		$apr = $res[0]['apr'];
		$principle = $res[0]['principle'];
		$interest = $res[0]['interest'];


		$pay_interest = $amount * $apr / 100;
		$pay_principle = $amount - $pay_interest;

		$rest_principle = $principle - $pay_principle;
		$rest_interest = $interest - $pay_interest;

		// echo '{"code":0, "data": "Success"}';

		$param = array (
				"contractid" => $loanid,
				"amount" => $amount,
				"userid" => $userid,
				"principle" => $pay_principle,
				"interest" => $pay_interest,
				"test" => $test
			);
		if ($db->insert("loan_repay", $param)) {
			$sql = "update loan_contract_v2 set principle=$rest_principle, interest=$rest_interest where id=$loanid";
			$db->sql($sql);

			echo '{"code":0, "data": "Success"}';
		}else {
			echo '{"code":1, "data": "Insert error"}';
		}

	}else {
		echo '{"code":1, "data": "Not found loan"}';
	}

	

	// echo repay($sectionid, $loanid, $amount);
}else if ($command=='wallet_transaction') {
	$sectionid = $_REQUEST['sectionid'];
	echo walletTransaction($sectionid);
}else if ($command=='wallet_ajax') {
	$sectionid = $_REQUEST['sectionid'];
	echo getwalletAjax($sectionid);
}else if ($command=='contract_detail') {
	$sectionid = $_REQUEST['sectionid'];
	$tokenid = $_REQUEST['tokenid'];
	echo contractDetail($sectionid, $tokenid);
}else if ($command=='contract_active') {
	$sectionid = $_REQUEST['sectionid'];
	$contractid = $_REQUEST['contractid'];

	$sql = "update loan_contract_v2 set principle=amount, interest=(amount*apr/100), status=2 where id=" . $contractid;

	if ($db->sql($sql)) {
		echo '{"code":0, "data": "Success"}';
	}else {
		echo '{"code":1, "data": "Update error"}';
	}

	// echo contractActive($sectionid, $contractid);
	
}else if ($command=='bank_select') {
	$country = $_REQUEST['country'];
	$userid = $_REQUEST['uid'];
	echo getBankFromCountry($country, $userid);
}else if ($command=='bank_select') {
	$country = $_REQUEST['country'];
	$userid = $_REQUEST['uid'];
	echo getBankFromCountry($country, $userid);
}else if ($command=='check_interest') {
	$sectionid = $_REQUEST['sectionid'];
	$contractid = $_REQUEST['contractid'];
	echo checkInterest($sectionid, $contractid);
}else if ($command=='next_period') {
	$sectionid = $_REQUEST['sectionid'];
	$contractid = $_REQUEST['contractid'];
	echo getNextPeriod($sectionid, $contractid);
}


?>