# RWA Tezos Jakarta

Real World Asset on Tezos Jakarta Chain. This is a testnet demo of the RWA Borrower Workflow and Infrastructure of on-chain tokenisation of real world asset. ShuttleOne has initially built an MVP on Ethereum in 2020 and put it into production in 2021 in partnership with Calista Finance. The protocol has tokenized 40mil of real world assets with $14mil of loans issued. A video of Calista Finance powered by ShuttleOne can be found here:

https://www.youtube.com/watch?v=AfuTBwvRJlk

**Getting Started**

Load 
https://shuttleone.network/rwa_jkt/login/login.php?e=f

Enter User Demo
Login:
user01@gmail.com
Password
qwe123

**Borrower Create Loan Application**
Please select loan details from list of Loan information

Credit Scoring Upload 
Company Registration Papers
2 months bank statement

Credit Scoring will appear for User Acceptance under Action "Submit" after "Legal" and "Audit" 2 layers of approval has approved asset tokenised for loan

Asset is Pledged to Shuttle One Pte Ltd and Tokenized into the Risk Assesement Token (RAT) under the RAT smart contract
https://gitlab.com/shuttle-one/rwa-tezos-jakarta/-/blob/main/smartcontract/RAT-Camel.mligo

**Borrower Accepts Credit Line**
Under "Credit Line" Tab, borrower sees loan details and "Start Loan"
Loan Contract is created under LoanProcess smart contract
https://gitlab.com/shuttle-one/rwa-tezos-jakarta/-/blob/main/smartcontract/LoanProcess.mligo

Asset value of loan is fractionalised into a fungible token Credit Application Token (CAT)
https://gitlab.com/shuttle-one/rwa-tezos-jakarta/-/blob/main/smartcontract/CAT-Pascal.mligo

Stablecoin swaps with CAT and is sent according to "Debt Level" into users' wallet and loan starts to calculate

**Borrower Repays Credit Line**
Borrower selects outstanding loan and repays directly from wallet in platform to liquidity pool providers

RAT and CAT is burnt and loan is completed

**Limitation of TestNet Demo**
Credit Scoring is automated, live DevOps credit scoring is connected to an OCR that reads and calculates loan documentation for credit scoring of loan to value ratio (LTV) for the purpose of demo it is automated for demo purposes

Liquidity pool on Tezos is not integrated as part of the grants milestone as ShuttleOne has been awaiting the USDT-TZ (USDT Tezos) stablecoin support in the Tezos ecosystem as we serve real world clientele who wants to counterparty to cash for their businesses
