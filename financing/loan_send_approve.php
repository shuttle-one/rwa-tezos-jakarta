<? include '../include/authen.php'; ?>
<? require_once ('../api/function.php'); ?>
<?

function moveFile($id, $filename) {

	$hasDir = false;
	$source = "../assets/docs/" . $filename;
	$directory = "../ipfs/ratdoc/d" . $id;

	if (is_dir($directory)) {
		$hasDir = true;
	}else 
	{
		if (mkdir($directory))
			$hasDir = true;
	}
	
	if ($hasDir) {
		$target = $directory . "/" . $filename;
		if (!copy($source, $target)) {
			// echo "Fail to copy ";
			$errors= error_get_last();
		    return "COPY ERROR: ".$errors['type'];
		}else 
			return "Success";

	}else 
		return "Cant make dir";
}


include '../include/database.php';
$db = new Database();  
$db->connect();

$id = $_REQUEST['id'];
$userid = $_SESSION['userid'];
$usertype = $_SESSION['usertype'];
$sectionid = $_SESSION['sectionid'];
$config = include ('../api/config.php');

$sql = "select * from loan_documents_v2 where userid=$userid and usertype=$usertype and id=$id and status=0";
if ($config['TEST']==1)
      $sql .= " and test=1 ";
  else $sql .= " and test=0 ";
$db->sql($sql);
$res = $db->getResult();

$success = 0;
$msg = "Submit success waiting for processing.";

$out = '{"code": "'.$success.'", "msg": "'.$msg.'"}';

if (count($res) == 1) { // CHECK DOCUMENTS IS OWN LOGIN USER.
	$docid = $res[0]['id'];
	$doctype = $res[0]['doc_type'];
	$ethaddress = $_SESSION['useraddress'];


	/*------ Check require all document ----*/
	if ($res[0]['file1']=='' ||
		$res[0]['file2']=='' ||
		$res[0]['file3']=='' ||
		$res[0]['file4']=='')
	{
		$success = 3;
		$msg = "This documents not complete enter field.Please upload all files";
		$out = '{"code": "'.$success.'", "msg": "'.$msg.'"}';
		echo $out;
		return;
	}

	$sql = "select * from loan_token where docid=$docid and userid=$userid";
	$db->sql($sql);
	$tokenRes = $db->getResult();


	if (count($tokenRes)>0) {
	// if (count($tokenRes)==1)
		$tokenid = $tokenRes[0]['tokenid'];
	// else if (count($tokenRes)>1) {

	// 	echo "Duplicate Docid ";// . $count($tokenRes);
	// 	return;
	// }
	} else {
		$success = 2;
		$msg = "Token Fail ". " : Not found docid : " . $docid;
		$out = '{"code": "'.$success.'", "msg": "'.$msg.'"}';
		echo $out;
		return;
		// echo "Token Fail ". " : Not found docid : " . $docid;
		// return;
	}

	/*------ START TEZOS CALL FOR MINT ------*/
	// $arr = callCreateDocTezos($_SESSION['useraddress'],  $tokenid, $docid);

	$arr = callCreateDocTezos($ethaddress,  $tokenid, $docid);
	if ($arr == '') {
		$success = 4;
		$msg = "Fail create doc ";
	}else {
		$sql = "update loan_documents_v2 set txhash='$arr',status=1 where id=$id";
		$db->sql($sql);
		// $msg = $msg . ' ' .$arr;
	}

	$out = '{"code": "'.$success.'", "msg": "'.$msg.'"}';


	/*------ STOP FOR DEBUG NEW FEATURE ------
	$arr = callCreateDoc($sectionid, $tokenid, $docid, $doctype, 'test');

	if ($arr['code']=="0")  {
		$txhash  = $arr['data']['txHash'];
		$sql = "update loan_documents_v2 set txhash='$txhash',status=1 where id=$id";
		$db->sql($sql);

		//----- COPY DOCS TO IPFS

		$file1 = $res[0]['file1'];
		$file2 = $res[0]['file2'];
		$file3 = $res[0]['file3'];
		$file4 = $res[0]['file4'];

		if ($file1 != '') 
			moveFile($id, $file1);
		if ($file2 != '') 
			moveFile($id, $file2);
		if ($file3!= '') 
			moveFile($id, $file3);
		if ($file4 != '') 
			moveFile($id, $file4);

		
		//----- CALL SEND EMAIL TO ADMIN FOR SUBMIT
		// $r = sendComitteeEmail("");
		// echo "Return from send email = $r";

	}

	else  {
		// $err = $arr['data'];
		$sql = "update loan_documents_v2 set txhash='$err' where id=$id";
		$db->sql($sql);
		$success = 0;
		$success = $arr['data'];
	}

	/*------ END FOR DEBUG NEW FEATURE ------*/
}

echo $out;

// echo $success;
// header("Location:document.php");

?>